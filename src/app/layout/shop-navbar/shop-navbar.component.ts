import { ShopModeService } from './../../service/shop-mode-service/shop-mode.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-shop-navbar',
  templateUrl: './shop-navbar.component.html',
  styleUrls: ['./shop-navbar.component.css']
})
export class ShopNavbarComponent implements OnInit, OnDestroy {
  destroy$: Subject<any>;

  activeList: any={
    'shop-home': true,
    'shop-profile': false
  };

  constructor(
    private router: Router,
    private shopModeService: ShopModeService
  ) {
    this.destroy$ = new Subject<any>();

    router.events.subscribe((event: any)=>{
      if(event instanceof NavigationEnd){
        let path = event.url.split('/');

        if(path.length > 2){
          //active icon
          this.activeIcon(path[3]);
        }
      }
    });
  }

  ngOnInit(): void {
  }

  activeIcon(type: string)
  {
    for(let key in this.activeList)
    {
      if(key === type){
        this.activeList[key] = true;
      }else{
        this.activeList[key] = false;
      }
    }
  }

  createPost()
  {
    this.shopModeService.openCreatePage();
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
