import { accountType, AuthService } from './auth/services/auth.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { first, catchError, throwError } from 'rxjs';
import { Response } from './model/interface/Response';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    private router: Router,
    private authService: AuthService
  ){

  }

  ngOnInit(): void {

  }

}
