import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProductPost } from 'src/app/model/interface/ProductPost';

@Component({
  selector: 'app-share-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css']
})
export class ProductInfoComponent implements OnInit {
  @Output('closeInfo') closeInfo: EventEmitter<boolean>;
  @Input('post') post?: ProductPost;
  @Input('isShowBack') isShowBack?: boolean = true;

  testLabels: string[]=[
    "Stock", "Type", "Rate"
  ];

  constructor() {
    this.closeInfo = new EventEmitter<boolean>();
  }

  ngOnInit(): void {
  }

  back()
  {
    this.closeInfo.emit(true);
  }

}
