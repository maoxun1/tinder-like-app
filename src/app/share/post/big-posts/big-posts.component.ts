import { SharePost } from './../../../model/interface/SharePost';
import { Component, Input, OnInit, ViewChild, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { ProductPost } from 'src/app/model/interface/ProductPost';
import Swiper, {SwiperOptions, Pagination, Virtual } from 'swiper';
import { EventsParams, SwiperComponent } from 'swiper/angular';

Swiper.use([Pagination, Virtual])
@Component({
  selector: 'app-share-big-posts',
  templateUrl: './big-posts.component.html',
  styleUrls: ['./big-posts.component.css']
})
export class BigPostsComponent implements OnInit, AfterViewInit{
  @Input('isBackBtn') isBackBtn: boolean = false;
  @Input('posts') posts: ProductPost[]|SharePost[];
  @Input('initialSlide') initSlide?: number;
  @Output('slideChange') slideChange: EventEmitter<number>;
  @Output('back') backBigPosts: EventEmitter<boolean>;

  @ViewChild('swiper') swiper?: SwiperComponent;

  isProductPosts(val: ProductPost[]|SharePost[]): val is ProductPost[] { return 'shopid' in val[0] }
  isSharePosts(val: ProductPost[]|SharePost[]): val is SharePost[] { return 'buyerid' in val[0] }
  parseTwoTypePosts(val: ProductPost[]|SharePost[]): (ProductPost|SharePost)[] {
    return this.posts as (ProductPost|SharePost)[];
  }

  config: SwiperOptions = {
    direction: "vertical",
    noSwipingClass: "description",
    pagination: true,
    virtual: true
  };

  constructor() {
    this.posts = [];
    this.slideChange = new EventEmitter<number>();
    this.backBigPosts = new EventEmitter<boolean>();
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    if(this.initSlide){
      this.swiper?.swiperRef.slideTo(this.initSlide);
    }
  }

  onSlideChange(event: [swiper: Swiper])
  {
    this.slideChange.emit(event[0].activeIndex);
  }

  back()
  {
    this.backBigPosts.emit(true);
  }
}
