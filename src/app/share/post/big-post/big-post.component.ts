import { SharePostLayoutService } from './../../../service/layout-service/share-post-layout.service';
import { ShareService } from 'src/app/service/share-service/share.service';
import { Buyer } from 'src/app/model/interface/Buyer';
import { SharePost } from './../../../model/interface/SharePost';
import { AuthService, accountState } from './../../../auth/services/auth.service';
import { ShoppingCartService } from 'src/app/service/shopping-cart-service/shopping-cart.service';
import { CommentService } from './../../../service/comment-service/comment.service';
import { PostService } from './../../../service/post-service/post.service';
import { BuyerService } from './../../../service/buyer-service/buyer.service';
import { NzCarouselComponent } from 'ng-zorro-antd/carousel';
import { Shop } from './../../../model/interface/Shop';
import { ShopService } from './../../../service/shop-service/shop.service';
import { map, Subject, take, takeUntil, mergeMap, Observable, repeatWhen, switchMapTo, BehaviorSubject, of, first, switchMap, tap, takeWhile, share, filter } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ProductPost, Share } from './../../../model/interface/ProductPost';
import { Component, Input, OnInit, Type, OnDestroy, ViewChild, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-share-big-post',
  templateUrl: './big-post.component.html',
  styleUrls: ['./big-post.component.css']
})
export class BigPostComponent implements OnInit, OnDestroy {
  //[ the index ]: the value
  iconState: {[key: string]: boolean} = {
    "like": false,
    "comment": false,
    "shop": false,
    "edit-post": false
  };

  @Input('isBackButton') isBackBtn: boolean = false;
  @Input('post') post?: ProductPost | SharePost ; // if sharePost, get the productpost first
  productPostOfSharePost?: ProductPost;
  @Output('back') backBigPost: EventEmitter<boolean>;

  @ViewChild('carousel') carousel!: NzCarouselComponent;

  bufferShareBigPost?: SharePost | null;
  isOpenBigSharePost: boolean = false;

  isSharePost(post: ProductPost|SharePost): post is SharePost { return 'buyerid' in post; }
  isProductPost(post: ProductPost|SharePost): post is ProductPost { return 'shopid' in post; }
  buyer$?: Observable<Buyer>;
  myBuyerid: string;

  destroy$: Subject<any>;
  likeCount$: Observable<number>;
  likeCountSubject: BehaviorSubject<any>;

  imgBase: string = environment.imgBase;

  shop$: Observable<Shop>;
  getCommentCount$: Observable<number>;

  isImgLoaded: boolean;
  isMoreText: boolean;
  picLoaded: number;
  isAddToCartOpened: boolean;

  isEditSharePostVisible: boolean = false;

  accountState: accountState;

  constructor(
    private shopService: ShopService,
    private buyerService: BuyerService,
    private postService: PostService,
    private commentService: CommentService,
    private cartService: ShoppingCartService,
    private authService: AuthService,
    private shareService: ShareService,
    private shareLayoutService: SharePostLayoutService
  ) {
    this.destroy$ = new Subject();
    this.isImgLoaded = false;
    this.isMoreText = false;
    this.isAddToCartOpened = false;
    this.picLoaded = 0;
    this.likeCountSubject = new BehaviorSubject<any>(true);
    this.likeCount$ = new Observable<number>();
    this.shop$ = new Observable<Shop>();
    this.getCommentCount$ = new Observable<number>();
    this.backBigPost = new EventEmitter<boolean>();
    this.accountState = authService.accountState;
    this.myBuyerid = buyerService.getBuyerId();
  }

  ngOnInit(): void {
    // if share post
    if(this.post && this.isSharePost(this.post)){
      this.buyer$ = this.buyerService.getBuyerById(this.post.buyerid);
      this.postService.getSpecificProductPost(this.post.itemid)
      .pipe(first())
      .subscribe(post=>{
        this.productPostOfSharePost = post;
        this.shop$ = this.shopService.getShop(this.productPostOfSharePost.shopid);
      });
    }else if(this.post && this.isProductPost(this.post)){
      this.shop$ = this.shopService.getShop(this.post.shopid);
    }

    // for buyer
    if(this.post && this.accountState.buyer){
      this.likeCount$ = this.likeCountSubject.pipe(
        switchMap(dummy=>{
          if(this.post){
            if(this.isProductPost(this.post)){
              return this.postService.getSpecificProductPost(this.post._id).pipe(
                takeUntil(this.destroy$),
                repeatWhen(this.likeCountSubject.asObservable),
                map(post=>post.likeCount)
              )
            }else{
              return this.shareService.getSpecificSharePost(this.post._id).pipe(
                takeUntil(this.destroy$),
                repeatWhen(this.likeCountSubject.asObservable),
                map(res => res.data.likeCount)
              );
            }
          }else{
            return of(0);
          }
        })
      );

      if(this.post){
        // product post
        if(this.isProductPost(this.post)){
          this.getCommentCount$ = this.commentService.getProductComments(this.post._id)
          .pipe(
            map(coms=>coms.length)
          );
        }else{
          //share post
          this.getCommentCount$ = this.commentService.getSharePostComments(this.post._id)
          .pipe(
            map(coms=>coms.length)
          );
        }
      }

      //set icon state
      this.buyerService.getBuyer().pipe(
        takeUntil(this.destroy$),
        switchMap(buyer=>{
          if(this.post){
            if(this.isProductPost(this.post)){
              return of(buyer.likedItems.includes(this.post._id));
            }else{
              return this.shareService.getLike(this.post._id).pipe(
                map(res=>{
                  if(res.data && this.post){
                    let isLiked = res.data.filter(x=>x.buyerid===this.buyerService.getBuyerId() && x.postid===this.post?._id);
                    return isLiked.length > 0;
                  }else{
                    return false;
                  }
                })
              );
            }
          }else{
            return of(false);
          }
        })
      ).subscribe(liked=>{
        if(liked) this.iconState["like"] = true;
        else this.iconState["like"]=false;
      });

      //add to cart page
      this.cartService.OnAddToCartPageChange()
      .pipe(takeUntil(this.destroy$))
      .subscribe(bool=>{
        this.isAddToCartOpened = bool;
      });
    }

    //open share post
    this.shareLayoutService.onOpenSharePost()
    .pipe(
      takeUntil(this.destroy$),
      filter(shareid=>shareid!=this.post?._id),
    ).subscribe(shareid=>{
      this.shareService.getSpecificSharePost(shareid)
      .pipe(first())
      .subscribe(res=>{
        this.bufferShareBigPost = res.data;
        this.isOpenBigSharePost = true;
      });
    });

    //on edit sharePost
    this.shareLayoutService.onOpenEditSharePostPanel()
    .pipe(takeUntil(this.destroy$))
    .subscribe(post=>{
      if(post && post._id===this.post?._id){
        this.isEditSharePostVisible = true;
      }else{
        this.isEditSharePostVisible = false;
      }
    });
  }

  backShareBigPost()
  {
    this.bufferShareBigPost = null;
    this.isOpenBigSharePost = false;
  }

  changeMode(type: string): void
  {
    this.iconState[type] = !this.iconState[type];
  }

  prevImg()
  {
    this.carousel.pre();
  }

  nextImg()
  {
    this.carousel.next();
  }

  imgLoaded()
  {
    this.picLoaded++;
    if(this.post){
      if(this.isProductPost(this.post) && this.picLoaded===this.post.images.length){
        this.isImgLoaded = true;
      }else if(this.isSharePost(this.post) && this.picLoaded===this.post.images.length+1){
        this.isImgLoaded = true;
      }
    }
  }

  moreText(event: boolean)
  {
    this.isMoreText = event;
  }

  likeAction()
  {
    //if liked -> unlike, else -> like
    let isLiked$ = this.buyerService.getBuyer().pipe(
      switchMap(buyer=>{
        if(!this.post){
          throw new Error('No post');
        }

        if(this.isProductPost(this.post)){
          return of(buyer.likedItems.includes(this.post._id));
        }else{
          return this.shareService.getLike(this.post._id).pipe(
            map(res=>{
              if(!this.post){
                throw new Error('No post');
              }
              if(!res.data){
                throw new Error('No response data');
              }
              let isLiked = res.data.filter(x=>x.buyerid===this.buyerService.getBuyerId() && x.postid===this.post?._id);
              return isLiked.length > 0;
            })
          );
        }
      })
    );

    let action$ = isLiked$.pipe(
      mergeMap(liked=>{
        if(!this.post) {
          return of(null);
        }

        if(liked){
          if(this.isProductPost(this.post)){
            return this.postService.unlikePost(this.post._id, this.buyerService.getBuyerId());
          }else{
            return this.shareService.unlike(this.buyerService.getBuyerId(), this.post._id);
          }

        }else{
          if(this.isProductPost(this.post)){
            return this.postService.likePost(this.post._id, this.buyerService.getBuyerId());
          }else{
            return this.shareService.like(this.buyerService.getBuyerId(), this.post._id);
          }
        }
      })
    );

    action$.subscribe(res=>{
      if(res){
        this.likeCountSubject.next(true);
      }
    });
  }

  closeActionPanel(type: string)
  {
    this.iconState[type] = false;
  }

  back()
  {
    this.backBigPost.emit(true);
  }

  backEditPanel()
  {
    this.isEditSharePostVisible = false;
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
