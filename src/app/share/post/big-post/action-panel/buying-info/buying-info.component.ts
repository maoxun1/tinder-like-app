import { BuyerService } from 'src/app/service/buyer-service/buyer.service';
import { ShoppingCartService } from './../../../../../service/shopping-cart-service/shopping-cart.service';
import { ProductPost } from './../../../../../model/interface/ProductPost';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-buying-info',
  templateUrl: './buying-info.component.html',
  styleUrls: ['./buying-info.component.css']
})
export class BuyingInfoComponent implements OnInit {
  @Input('post') post?:ProductPost
  @Output('back') close: EventEmitter<boolean>;

  constructor(
    private cartService: ShoppingCartService,
    private buyerService: BuyerService
  ) {
    this.close = new EventEmitter<boolean>();
  }

  ngOnInit(): void {
  }

  isBuyer()
  {
    return this.buyerService.getBuyerId() ? true : false;
  }

  back()
  {
    this.close.emit(true);
  }

  openAddToCart()
  {
    this.cartService.openAddToCartPage();
  }
}
