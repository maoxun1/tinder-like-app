import { SharePost } from './../../../../model/interface/SharePost';
import { ProductPost } from './../../../../model/interface/ProductPost';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-action-panel',
  templateUrl: './action-panel.component.html',
  styleUrls: ['./action-panel.component.css']
})
export class ActionPanelComponent implements OnInit {
  @Output('close') closeAction: EventEmitter<string>;
  @Input('actionType') actionType: "comment" | "shop" | 'edit-post' | '' = "";
  @Input('post') post?: ProductPost | SharePost;
  @Input('productPostOfSharePost') productPostOfSharePost?: ProductPost;

  isSharePost(post: ProductPost|SharePost): post is SharePost { return 'buyerid' in post; }
  isProductPost(post: ProductPost|SharePost): post is ProductPost { return 'shopid' in post; }

  constructor() {
    this.closeAction = new EventEmitter<string>();
  }

  ngOnInit(): void {
  }

  close()
  {
    this.closeAction.emit(this.actionType);
  }
}
