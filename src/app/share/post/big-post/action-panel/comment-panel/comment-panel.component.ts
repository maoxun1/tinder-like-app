import { BuyerService } from 'src/app/service/buyer-service/buyer.service';
import { SharePost } from './../../../../../model/interface/SharePost';
import { ProductPost } from './../../../../../model/interface/ProductPost';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-comment-panel',
  templateUrl: './comment-panel.component.html',
  styleUrls: ['./comment-panel.component.css']
})
export class CommentPanelComponent implements OnInit {
  // maybe Product or share post, so put in post directly
  @Input('post') post?: ProductPost | SharePost;

  activeOpt: {[key: string]: boolean};

  constructor(
    private buyerService: BuyerService
  ) {
    this.activeOpt = {
      "comment": true,
      "shared": false
    };
  }

  ngOnInit(): void {
    if(!this.isBuyer()){
      this.activeOpt = {
        "comment": false,
        "shared": true
      };
    }
  }

  isBuyer()
  {
    return this.buyerService.getBuyerId() ? true : false ;
  }

  changeMode(type: "comment" | "shared")
  {
    for(let key in this.activeOpt)
    {
      this.activeOpt[key] = false;
      if(key==type){
        this.activeOpt[type] = true;
      }
    }
  }

}
