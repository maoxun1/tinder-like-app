import { SharePostLayoutService } from './../../../../../../service/layout-service/share-post-layout.service';
import { BuyerService } from './../../../../../../service/buyer-service/buyer.service';
import { Observable, switchMap, map, first, lastValueFrom, switchMapTo, of, shareReplay } from 'rxjs';
import { ShareService } from 'src/app/service/share-service/share.service';
import { SharePost } from './../../../../../../model/interface/SharePost';
import { ProductPost } from './../../../../../../model/interface/ProductPost';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-shared',
  templateUrl: './shared.component.html',
  styleUrls: ['./shared.component.css']
})
export class SharedComponent implements OnInit {
  @Input('post') post?: ProductPost | SharePost; // only product post can be shared

  shares?: SharePost[];
  buyerPics: string[]=[];
  getShares$?: Observable<SharePost[]>

  isSharePost(post: ProductPost|SharePost): post is SharePost { return 'buyerid' in post; }
  isProductPost(post: ProductPost|SharePost): post is ProductPost { return 'shopid' in post; }

  constructor(
    private shareService: ShareService,
    private shareLayoutService: SharePostLayoutService,
    private buyerService: BuyerService
  ) {

  }

  ngOnInit(): void {
    if(this.post){
      this.getShares$ = this.shareService.getSharePostsOfProductPost({
        itemid: this.isProductPost(this.post) ? this.post._id : this.post.itemid,
        skip: 0,
        limit: 0
      });

      this.getShares$.subscribe(async posts=>{
        this.shares  = posts;
        for(let share of posts){
          this.buyerPics.push(await this.getBuyerPic(share.buyerid));
        }
      });

    }
  }

  async getBuyerPic(id: string): Promise<string>
  {
    let pic = await lastValueFrom(
      this.buyerService.getBuyerById(id)
      .pipe(
        first(),
        switchMap(b=>of(b.profilePic))
      )
    )

    return pic;
  }

  navigateToSharePost(shareid: string)
  {
    this.shareLayoutService.openSharePost(shareid);
  }
}
