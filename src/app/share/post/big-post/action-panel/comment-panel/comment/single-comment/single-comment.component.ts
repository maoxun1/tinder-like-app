import { SharePostCommentDisplayDTO, SharePostCommentLikeDTO } from './../../../../../../../model/DTO/SharePostComment';
import { ProductCommentLikeDTO, ProductPostCommentDisplayDTO } from './../../../../../../../model/DTO/ProductComment';
import { Buyer } from './../../../../../../../model/interface/Buyer';
import { Observable, first } from 'rxjs';
import { ProductCommentDTO } from 'src/app/model/DTO/ProductComment';
import { CommentService } from 'src/app/service/comment-service/comment.service';
import { BuyerService } from './../../../../../../../service/buyer-service/buyer.service';
import { Component, Input, OnInit } from '@angular/core';
import { ProductPostComment } from 'src/app/model/interface/ProductPostComment';

@Component({
  selector: 'app-single-comment',
  templateUrl: './single-comment.component.html',
  styleUrls: ['./single-comment.component.css']
})
export class SingleCommentComponent implements OnInit {
  @Input('comment') comment?: ProductPostCommentDisplayDTO|SharePostCommentDisplayDTO;
  @Input('isProductPost') isProductPost?: boolean;

  constructor(
    private buyerService: BuyerService,
    private commentService: CommentService
  ) {
  }

  ngOnInit(): void {

  }

  like()
  {
    var like$ = null;
    if(this.isProductPost===true){
      like$ = this.commentService.likeProductComment(<ProductCommentLikeDTO>{
        commentId: this.comment?._id,
        userId: this.buyerService.getBuyerId()
      }).pipe(
        first()
      );
    }else{
      like$ = this.commentService.likeSharePostComment(<SharePostCommentLikeDTO>{
        commentId: this.comment?._id,
        userId: this.buyerService.getBuyerId()
      }).pipe(
        first()
      );
    }

    like$.subscribe(like=>{
      //front-end only
      if(this.comment){
        this.comment.isLiked = !this.comment.isLiked;
        this.comment.likeCount = this.comment.isLiked ? this.comment.likeCount+1 : this.comment.likeCount-1 ;
      }
    });
  }

}
