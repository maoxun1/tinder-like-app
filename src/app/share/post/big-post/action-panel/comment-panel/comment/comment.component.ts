import { SharePostCommentDisplayDTO } from './../../../../../../model/DTO/SharePostComment';
import { ProductPostCommentDisplayDTO } from './../../../../../../model/DTO/ProductComment';
import { ProductPostComment } from './../../../../../../model/interface/ProductPostComment';
import { Observable, first, Subject, switchMap, BehaviorSubject, debounce, debounceTime, tap, of } from 'rxjs';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ProductCommentDTO } from 'src/app/model/DTO/ProductComment';
import { CommentService } from 'src/app/service/comment-service/comment.service';
import { ProductPost } from 'src/app/model/interface/ProductPost';
import { SharePost } from 'src/app/model/interface/SharePost';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  @Input('post') post?: ProductPost | SharePost;

  activeOpt: {[key: string]: boolean};
  sendText: string="";

  commentRetrigger: BehaviorSubject<any>;
  getComment$: Observable<ProductPostCommentDisplayDTO[]|SharePostCommentDisplayDTO[]>;

  isSharePost(post: ProductPost|SharePost): post is SharePost { return 'buyerid' in post; }
  isProductPost(post: ProductPost|SharePost): post is ProductPost { return 'shopid' in post; }
  parseTwoTypeComments(c: ProductPostCommentDisplayDTO[]|SharePostCommentDisplayDTO[])
  {
    return c as (ProductPostCommentDisplayDTO|SharePostCommentDisplayDTO)[];
  }

  constructor(
    private commentService: CommentService
  ) {
    this.activeOpt = {
      "comment": true,
      "shared": false
    };
    this.getComment$ = new Observable<ProductPostCommentDisplayDTO[]|SharePostCommentDisplayDTO[]>();
    this.commentRetrigger = new BehaviorSubject<any>(null);
  }

  ngOnInit(): void {
    this.getComment$ = this.commentRetrigger.pipe(
      switchMap(dummy=>{
        if(this.post){
          if(this.isProductPost(this.post)){
            return this.commentService.getProductComments(this.post._id);
          }else{
            return this.commentService.getSharePostComments(this.post._id);
          }
        }else{
          return of([])
        }
      })
    );
  }

  changeMode(type: "comment" | "shared")
  {
    for(let key in this.activeOpt)
    {
      this.activeOpt[key] = false;
      if(key==type){
        this.activeOpt[type] = true;
      }
    }
  }

  send()
  {
    if(!this.post){
      return;
    }

    if(this.isProductPost(this.post)){
      this.commentService.sendComment(this.post._id, this.sendText)
      .pipe(
        first(),
        debounceTime(1000)
      )
      .subscribe(com=>{
        //retrigger
        this.sendText = "";
        this.commentRetrigger.next(null);
      });
    }else{
      this.commentService.sendSharePostComment(this.post._id, this.sendText)
      .pipe(
        first(),
        debounceTime(1000)
      ) .subscribe(com=>{
        //retrigger
        this.sendText = "";
        this.commentRetrigger.next(null);
      });
    }

  }
}
