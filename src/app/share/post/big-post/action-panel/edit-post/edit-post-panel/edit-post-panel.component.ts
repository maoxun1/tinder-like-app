import { first } from 'rxjs';
import { ShareService } from 'src/app/service/share-service/share.service';
import { SharePost } from './../../../../../../model/interface/SharePost';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, Input, OnInit, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-edit-post-panel',
  templateUrl: './edit-post-panel.component.html',
  styleUrls: ['./edit-post-panel.component.css']
})
export class EditPostPanelComponent implements OnInit {
  @Input('post') post?: SharePost;
  @Output('back') backAction: EventEmitter<any>;

  editForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.maxLength(60)]),
    caption: new FormControl('', [Validators.required, Validators.maxLength(300)])
  });

  public get name() { return this.editForm.get('name') }
  public get caption() { return this.editForm.get('caption') }

  isUpdating: boolean = false;

  constructor(
    private shareService: ShareService
  ) {
    this.backAction = new EventEmitter<any>();
  }

  ngOnInit(): void {
    if(this.post){
      this.editForm.patchValue({
        name: this.post.name,
        caption: this.post.content
      });
    }
  }

  confirm()
  {
    if(this.editForm.invalid){
      alert("invalid content!");
      return;
    }

    if(this.post){
      this.isUpdating = true;
      this.shareService.patchPost(this.post._id, this.name?.value, this.caption?.value)
      .pipe(first())
      .subscribe(post => {
        if(post){
          alert("Edit post successfully");
        }

        this.isUpdating = false;
        location.reload();
      });
    }
  }

  back()
  {
    this.backAction.emit(null);
  }
}
