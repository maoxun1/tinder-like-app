import { SharePostLayoutService } from './../../../../../service/layout-service/share-post-layout.service';
import { first } from 'rxjs';
import { ShareService } from 'src/app/service/share-service/share.service';
import { SharePost } from 'src/app/model/interface/SharePost';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

enum actionType {
  delete = 'delete',
  edit = 'edit'
}

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {
  @Input('post') post?: SharePost;

  action: typeof actionType = actionType;

  isModalVisible: boolean = false;

  constructor(
    private shareService: ShareService,
    private sharePostLayoutService: SharePostLayoutService
  ) {
  }

  ngOnInit(): void {
  }

  doAction(type: actionType)
  {
    switch(type)
    {
      case actionType.delete:
        this.isModalVisible = true;
        break;

      case actionType.edit:
        if(this.post){
          this.sharePostLayoutService.openEditSharePost(this.post);
        }
        break;
    }
  }

  deletePost()
  {
    if(this.post){
      this.shareService.deletePost(this.post._id)
      .pipe(first())
      .subscribe(res=>{
        this.isModalVisible = false;
        alert(res.msg);
        location.reload();
      })
    }
  }
}
