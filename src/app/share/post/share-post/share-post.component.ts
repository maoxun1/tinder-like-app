import { HttpErrorResponse } from '@angular/common/http';
import { PostService } from 'src/app/service/post-service/post.service';
import { Observable, of, switchMap, switchMapTo, first, pipe, catchError, throwError } from 'rxjs';
import { SharePost } from './../../../model/interface/SharePost';
import { environment } from 'src/environments/environment';
import { ProductPost } from './../../../model/interface/ProductPost';
import { Component, Input, OnInit, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'app-share-share-post',
  templateUrl: './share-post.component.html',
  styleUrls: ['./share-post.component.css']
})
export class SharePostComponent implements OnInit {
  @Input('post') post?: SharePost|ProductPost; //to get product info

  @Input('coverImg') cover?:string;
  @Input('isBigPost') isBigPost?: boolean;

  isSharePost(val: SharePost|ProductPost): val is SharePost { return 'buyerid' in val; }
  isProductPost(val: SharePost|ProductPost): val is ProductPost { return 'shopid' in val; }

  isProductPostDeleted: boolean;

  constructor(
    private postService: PostService
  ) {
    this.isProductPostDeleted = false;
  }

  ngOnInit(): void {
    //need to get the item image
    if(!this.cover && this.post && this.isProductPost(this.post)){
      this.cover=this.post.images[0];
      this.cover = this.filterCoverImage(this.cover);
    }

    // getPrice$ keep sending request -> maybe is swiper's problem
    if(this.post && this.isSharePost(this.post)){
      this.postService.getSpecificProductPost(this.post.itemid)
      .pipe(
        first(),
        catchError(err=>{
          if(err instanceof HttpErrorResponse && err.status==404){
            //no post, maybe deleted productpost
            this.isProductPostDeleted = true;
            if(this.post){
              this.cover = this.post.images[0];
              this.cover = this.filterCoverImage(this.cover);
            }
          }

          return throwError(()=>err);
        })
      )
      .subscribe(post=>{
        this.cover = post.images[0];
        this.cover = this.filterCoverImage(this.cover);
      });
    }

  }

  filterCoverImage(url: string): string
  {
    var image = url;
    if(!url.startsWith('http') && !url.startsWith('https')){
      image = environment.imgBase+url;
    }
    return image;
  }

}
