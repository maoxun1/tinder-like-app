import { EditPostPanelComponent } from './post/big-post/action-panel/edit-post/edit-post-panel/edit-post-panel.component';
import { SafeUrlPipe } from './../pipes/safe-url.pipe';
import { CommentTimePipe } from './../pipes/comment-time.pipe';
import { EntireLoaderRollerComponent } from './../layout/entire-loader-roller/entire-loader-roller.component';
import { MoreContentDirective } from './../directives/more-content/more-content.directive';
import { ImgloadDirective } from '../directives/imgload/imgload.directive';
import { LoaderSpinerComponent } from './../layout/loader-spiner/loader-spiner.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostComponent } from './post/post.component';
import { BigPostComponent } from './post/big-post/big-post.component';
import { SharePostComponent } from './post/share-post/share-post.component';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';
import { BigPostsComponent } from './post/big-posts/big-posts.component';
import { SwiperModule } from 'swiper/angular';
import { ActionPanelComponent } from './post/big-post/action-panel/action-panel.component';
import { BuyingInfoComponent } from './post/big-post/action-panel/buying-info/buying-info.component';
import { CommentComponent } from './post/big-post/action-panel/comment-panel/comment/comment.component';
import { CommentPanelComponent } from './post/big-post/action-panel/comment-panel/comment-panel.component';
import { SharedComponent } from './post/big-post/action-panel/comment-panel/shared/shared.component';
import { SingleCommentComponent } from './post/big-post/action-panel/comment-panel/comment/single-comment/single-comment.component';
import { BuyerComponent } from './buyer/buyer.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { CartItemComponent } from './shopping-cart/cart-item/cart-item.component';
import { ProductInfoComponent } from './product-info/product-info.component';
import { AddToCartComponent } from './shopping-cart/add-to-cart/add-to-cart.component';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NumButtonComponent } from './shopping-cart/num-button/num-button.component';
import { BuyerProfileComponent } from './buyer/buyer-profile/buyer-profile.component';
import { FollowComponent } from './buyer/buyer-profile/follow/follow.component';
import { ShopComponent } from './shop/shop.component';
import { ShopProfileComponent } from './shop/shop-profile/shop-profile.component';
import { EditPostComponent } from './post/big-post/action-panel/edit-post/edit-post.component';
import { NzModalModule } from 'ng-zorro-antd/modal';

const exportComponents:any[] = [
  PostComponent,
  BigPostComponent,
  SharePostComponent,
  LoaderSpinerComponent,
  EntireLoaderRollerComponent,
  BigPostsComponent,
  BuyerComponent,
  ShoppingCartComponent,
  ProductInfoComponent,
  AddToCartComponent,
  BuyerProfileComponent,
  FollowComponent,
  CommentComponent,
  ShopComponent,
  ShopProfileComponent,
  SharedComponent
];
const exportModules:any[] = [
  FormsModule,
  NzIconModule,
  SwiperModule
];
const exportDirectives: any[]=[
  ImgloadDirective,
  MoreContentDirective
]
const exportPipes: any[]=[
  CommentTimePipe,
  SafeUrlPipe
]

@NgModule({
  declarations: [
    PostComponent,
    BigPostComponent,
    SharePostComponent,
    LoaderSpinerComponent,
    ImgloadDirective,
    MoreContentDirective,
    EntireLoaderRollerComponent,
    BigPostsComponent,
    ActionPanelComponent,
    BuyingInfoComponent,
    CommentPanelComponent,
    SharedComponent,
    CommentComponent,
    SingleCommentComponent,
    CommentTimePipe,
    BuyerComponent,
    ShoppingCartComponent,
    CartItemComponent,
    ProductInfoComponent,
    AddToCartComponent,
    NumButtonComponent,
    BuyerProfileComponent,
    FollowComponent,
    ShopComponent,
    ShopProfileComponent,
    SafeUrlPipe,
    EditPostComponent,
    EditPostPanelComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NzIconModule,
    NzCarouselModule,
    SwiperModule,
    NzCheckboxModule,
    NzModalModule,
    ReactiveFormsModule,
  ],
  exports:[
    exportComponents,
    exportModules,
    exportDirectives,
    exportPipes
  ]
})
export class ShareModule { }
