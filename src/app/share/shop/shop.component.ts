import { Component, Input, OnInit } from '@angular/core';
import { Shop } from 'src/app/model/interface/Shop';

@Component({
  selector: 'app-share-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {
  @Input('shop') shop?: Shop;

  constructor() { }

  ngOnInit(): void {
  }

}
