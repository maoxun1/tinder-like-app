import { ShopModeService } from './../../../service/shop-mode-service/shop-mode.service';
import { Shop } from './../../../model/interface/Shop';
import { Component, Input, OnInit, OnDestroy, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject, first, Observable, Subject, takeUntil, switchMap, map } from 'rxjs';
import { ProductPost } from 'src/app/model/interface/ProductPost';
import { PostService } from 'src/app/service/post-service/post.service';
import { ProfileLayoutService } from 'src/app/service/layout-service/profile-layout.service';
import SwiperCore, { SwiperOptions } from 'swiper';

@Component({
  selector: 'app-share-shop-profile',
  templateUrl: './shop-profile.component.html',
  styleUrls: ['./shop-profile.component.css']
})
export class ShopProfileComponent implements OnInit, OnDestroy, OnChanges {
  @Input('shop') shop?: Shop;
  @Output('openBigProductPost') openBigProductPostAction: EventEmitter<{
    posts: ProductPost[],
    init: number
  }>;

  destroy$: Subject<any>;

  shopPosts$?: Observable<ProductPost[]>;
  shopPostsRetrigger$?: BehaviorSubject<any>;

  shopPosts: ProductPost[][];
  testPost$: Observable<ProductPost[]>;

  swiperConfig: SwiperOptions = {
    pagination: {
      dynamicBullets: true,
      dynamicMainBullets: 3 
    },
  };

  constructor(
    private postService: PostService,
    private profileLayoutService: ProfileLayoutService,
    private shopModeService: ShopModeService
  ) {
    this.destroy$ = new Subject<any>();
    this.shopPostsRetrigger$ = new BehaviorSubject<any>(null);
    this.shopPosts = [];
    this.openBigProductPostAction = new EventEmitter<{posts: ProductPost[], init: number}>();

    this.testPost$ = postService.getProductPostsRandomly(4).pipe(first());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['shop']){
      this.shopPosts$ = this.shopPostsRetrigger$?.pipe(
        takeUntil(this.destroy$),
        switchMap(dummy=>{
          if(this.shop){
            return this.shopModeService.getProductPostsOfShop(this.shop._id).pipe(map(res=>res.data));
          }else{
            return [];
          }
        })
      )
    }
  }

  ngOnInit(): void {
    if(this.shopPosts$){
      this.shopPosts$.subscribe(posts=>{
        this.shopPosts = this.sliceSharePost(posts);
      })
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  openBigProductPost(post: ProductPost)
  {
    this.openBigProductPostAction.emit({
      posts: this.shopPosts.flat(),
      init: this.shopPosts.flat().indexOf(post)
    });
  }

  sliceSharePost(posts: ProductPost[]): ProductPost[][]
  {
    posts = posts.sort((x, y)=>(new Date(y.createdAt)).getTime()-(new Date(x.createdAt)).getTime());
    var sliced: ProductPost[][] = [];
    var tmp: ProductPost[] = [];
    for(let post of posts){
      if(tmp.length==4){
        sliced.push(tmp);
        tmp=[];
      }
      tmp.push(post);
    }
    if(tmp.length>0){
      sliced.push(tmp);
      tmp=[];
    }
    return sliced;
  }
}
