import { AuthService } from 'src/app/auth/services/auth.service';
import { FollowService } from './../../../../service/follow-service/follow.service';
import { Shop } from 'src/app/model/interface/Shop';
import { FollowReturnDto } from 'src/app/model/DTO/Follow';
import { Subject, takeUntil, Observable, first, BehaviorSubject, switchMap, of, tap, switchMapTo } from 'rxjs';
import { FollowPageFactor, followPageType, ProfileLayoutService } from './../../../../service/layout-service/profile-layout.service';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Buyer } from 'src/app/model/interface/Buyer';
import { Follow } from 'src/app/model/interface/Follow';

@Component({
  selector: 'app-follow',
  templateUrl: './follow.component.html',
  styleUrls: ['./follow.component.css']
})
export class FollowComponent implements OnInit, OnDestroy {
  @Input('userid') userid?: string;
  @Input('isSearchPage') isSearchPage?: boolean = true;

  factor?: FollowPageFactor;
  follows$?: Observable<FollowReturnDto[]>;
  followsRetrigger$: BehaviorSubject<any>;
  follows: FollowReturnDto[];

  parseBuyer(val: any) { return val as Buyer; }
  parseShop(val: any) { return val as Shop; }

  destroy$: Subject<any>;

  constructor(
    private profileLayoutService: ProfileLayoutService,
    private followService: FollowService,
    private authService: AuthService
  ) {
    this.destroy$ = new Subject<any>();
    this.followsRetrigger$ = new BehaviorSubject<any>(null);
    this.follows = [];
  }

  ngOnInit(): void {
    this.profileLayoutService.onFollowPageChange()
    .pipe(takeUntil(this.destroy$))
    .subscribe(factor=>{
      this.factor = factor;
    });

    //set follow observable
    this.follows$ = this.followsRetrigger$
    .pipe(
      takeUntil(this.destroy$),
      switchMap(dummy=>{
        if(this.userid && this.factor){
          if(this.factor.type==='following'){
            return this.followService.getFollowings({
              userid: this.userid,
              skip: 0,
              limit: 0 //no limit
            });
          }else{
            return this.follows$ = this.followService.getFollowers({
              userid: this.userid,
              skip: 0,
              limit: 0 //no limit
            })
          }
        }else{
          return of([]);
        }
      })
    )

    this.follows$
    .pipe(takeUntil(this.destroy$))
    .subscribe(dto=>{
      this.follows = dto;
    });
  }

  back()
  {
    if(this.factor){
      this.profileLayoutService.closeFollow(this.factor.type);
    }
  }

  doAction(type: followPageType, followingid: string, followInfo: Follow)
  {
    if(this.userid){
      if(type==='following'){
        this.followService.unfollow({
          followerid: this.userid,
          followingid: followingid,
          followType: followInfo.followType
        })
        .pipe(first())
        .subscribe(follow=>{
          //retrigger
          this.followsRetrigger$.next(null);
        });
      }else{
        //remove from others
        this.followService.unfollow({
          followerid: followingid,
          followingid: this.userid,
          followType: followInfo.followType
        })
        .pipe(first())
        .subscribe(follow=>{
          this.followsRetrigger$.next(null);
        });
      }
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
