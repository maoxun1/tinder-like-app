import { SharePostComponent } from './../../post/share-post/share-post.component';
import { ShareService } from 'src/app/service/share-service/share.service';
import { SharePost } from './../../../model/interface/SharePost';
import { ProfileLayoutService } from './../../../service/layout-service/profile-layout.service';
import { Component, OnInit, Input, OnDestroy, OnChanges, SimpleChanges, Output, EventEmitter, ViewChild, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { Observable, mergeMap, of, Subject, first, switchMap, takeUntil, BehaviorSubject, switchMapTo } from 'rxjs';
import { Buyer } from 'src/app/model/interface/Buyer';
import { ProductPost } from 'src/app/model/interface/ProductPost';
import { BuyerService } from 'src/app/service/buyer-service/buyer.service';
import { PostService } from 'src/app/service/post-service/post.service';
import SwiperCore, { SwiperOptions } from 'swiper';

@Component({
  selector: 'app-buyer-profile',
  templateUrl: './buyer-profile.component.html',
  styleUrls: ['./buyer-profile.component.css']
})
export class BuyerProfileComponent implements OnInit, OnDestroy, OnChanges {
  @ViewChildren(SharePostComponent) sharePostsView?: QueryList<SharePostComponent>;
  @Input('buyer') buyer?: Buyer;
  @Output('openSharePosts') openSharePosts: EventEmitter<{
    posts: SharePost[],
    init: number
  }>;
  buyerSharePosts$?: Observable<SharePost[]>
  buyerSharePostRetrigger$: BehaviorSubject<any>;
  sharePosts: SharePost[][];

  destroy$: Subject<any>;

  isModalVisible: boolean = false;
  willDeletePost: SharePost|null = null;

  swiperConfig: SwiperOptions = {
    pagination: {
      dynamicBullets: true,
      dynamicMainBullets: 3
    },
  };

  constructor(
    private buyerService: BuyerService,
    private postService: PostService,
    private profileLayoutService: ProfileLayoutService,
    private shareService: ShareService
  ) {
    this.destroy$ = new Subject<any>();
    this.sharePosts = [];
    this.openSharePosts = new EventEmitter<{
      posts: SharePost[],
      init: number
    }>();
    this.buyerSharePostRetrigger$ = new BehaviorSubject<any>(null);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['buyer']){
      if(this.buyer){
        if(!this.buyerSharePosts$){
          this.buyerSharePosts$ = this.buyerSharePostRetrigger$.pipe(
            takeUntil(this.destroy$),
            switchMapTo(
              this.shareService.getSharePostsOfBuyer({
                buyerid: this.buyer._id,
                skip: 0,
                limit: 0
              }).pipe(first())
            )
          )
        }else{
          // retrigger new state like, sharepost like count
          this.buyerSharePostRetrigger$.next(null);
        }
      }
    }
  }

  ngOnInit(): void {
    if(this.buyerSharePosts$)
    {
      this.buyerSharePosts$.subscribe(posts=>{
        this.sharePosts = this.sliceSharePost(posts);
      });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  sliceSharePost(posts: SharePost[]): SharePost[][]
  {
    posts = posts.sort((x, y)=>(new Date(y.createdAt)).getTime()-(new Date(x.createdAt)).getTime());
    var sliced: SharePost[][] = [];
    var tmp: SharePost[] = [];
    for(let post of posts){
      if(tmp.length==4){
        sliced.push(tmp);
        tmp=[];
      }
      tmp.push(post);
    }
    if(tmp.length>0){
      sliced.push(tmp);
      tmp=[];
    }
    return sliced;
  }

  // open on the profile page
  openFollowing()
  {
    this.profileLayoutService.openFollow("following");
  }

  openFollower()
  {
    this.profileLayoutService.openFollow("follower");
  }

  openBigSharePost(init: SharePost)
  {
    //if deleted, cannot open
    if(this.sharePostsView){
      let post = this.sharePostsView.find(x=>x.post?._id==init._id);
      if(post?.isProductPostDeleted) {
        // click and delete post
        if(this.buyer?._id === this.buyerService.getBuyerId()){
          this.willDeletePost = post.post as SharePost;
          this.isModalVisible = true;
        }

        return;
      }
    }

    this.openSharePosts.emit({
      posts: this.sharePosts.flat(),
      init: this.sharePosts.flat().indexOf(init)
    });
  }

  deletePost()
  {
    if(this.willDeletePost){
      this.shareService.deletePost(this.willDeletePost._id)
      .pipe(first())
      .subscribe(res=>{
        this.willDeletePost = null;
        this.isModalVisible = false;
        alert(res.msg);
        this.buyerSharePostRetrigger$.next(null);
      })
    }

  }
}
