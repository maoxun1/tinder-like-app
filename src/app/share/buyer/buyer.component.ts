import { Component, Input, OnInit } from '@angular/core';
import { Buyer } from 'src/app/model/interface/Buyer';

@Component({
  selector: 'app-share-buyer',
  templateUrl: './buyer.component.html',
  styleUrls: ['./buyer.component.css']
})
export class BuyerComponent implements OnInit {
  @Input('buyer') buyer?: Buyer;

  constructor() { }

  ngOnInit(): void {
  }

}
