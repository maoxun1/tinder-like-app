import { first } from 'rxjs';
import { ShoppingCartService } from 'src/app/service/shopping-cart-service/shopping-cart.service';
import { NumButtonComponent } from './../num-button/num-button.component';
import { environment } from './../../../../environments/environment.prod';
import { CartItem } from './../../../model/interface/CartItem';
import { Component, Input, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';


@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.css']
})
export class CartItemComponent implements OnInit, AfterViewInit {
  @Input('item') cartItem?: CartItem;
  @ViewChild('numBtn') numBtn?: ElementRef<NumButtonComponent>;

  imgBase: string = environment.imgBase;
  checked: boolean = false;

  constructor(
    private cartService: ShoppingCartService
  ) { }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    if(this.cartItem && this.numBtn){
      (this.numBtn as any as NumButtonComponent).itemNumber = this.cartItem.quantity;
    }
  }

  handleAdd(currentNum: number)
  {
    if(this.cartItem){
      this.cartService.updateItemQuantityInCart(this.cartItem, currentNum)
      .pipe(first())
      .subscribe(cart=>{
        if(!cart){
          alert("cannot modify amount!");
        }
      });
    }
  }

  handleReduce(currentNum: number)
  {
    if(this.cartItem){
      this.cartService.updateItemQuantityInCart(this.cartItem, currentNum)
      .pipe(first())
      .subscribe(cart=>{
        if(!cart){
          alert("cannot modify amount!");
        }
      });
    }
  }

  handleCheck()
  {
    if(!this.cartItem){
      return;
    }

    if(this.checked){
      this.cartService.addItemToCheckStack(this.cartItem);
    }else{
      this.cartService.removeItemFromCheckStack(this.cartItem);
    }
  }
}
