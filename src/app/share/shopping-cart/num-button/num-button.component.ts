import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-num-button',
  templateUrl: './num-button.component.html',
  styleUrls: ['./num-button.component.css']
})
export class NumButtonComponent implements OnInit {
  @Output('add') addOutput: EventEmitter<number>;
  @Output('mius') minusOutput: EventEmitter<number>;

  private _itemNumber: number = 1;

  public get itemNumber() { return this._itemNumber; }
  public set itemNumber(val: number) {
    if(val > 0){
      this._itemNumber = val
    }
 }

  constructor() {
    this.addOutput = new EventEmitter<number>();
    this.minusOutput = new EventEmitter<number>();
  }

  ngOnInit(): void {
  }

  add()
  {
    this._itemNumber++;
    this.addOutput.emit(this._itemNumber);
  }

  minus()
  {
    if(this._itemNumber===1){
      return;
    }
    this._itemNumber--;
    this.minusOutput.emit(this._itemNumber);
  }
}
