import { OrderPostDTO, OrderItem } from './../../model/DTO/OrderPost';
import { BuyerService } from './../../service/buyer-service/buyer.service';
import { OrderService } from './../../service/order-service/order.service';
import { Router } from '@angular/router';
import { totalInfo } from './../../service/shopping-cart-service/shopping-cart.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable, first, tap, catchError, of, throwError } from 'rxjs';
import { CartItem } from 'src/app/model/interface/CartItem';
import { ShoppingCartService } from 'src/app/service/shopping-cart-service/shopping-cart.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-share-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  @Output('back') back: EventEmitter<boolean>;

  isPlacedOrder: boolean;

  onCartChange$: Observable<CartItem[]>;
  totalInfo$: Observable<totalInfo>;

  constructor(
    private cartService: ShoppingCartService,
    private buyerService: BuyerService,
    private orderService: OrderService,
    private router: Router
  ) {
    this.back = new EventEmitter<boolean>();
    this.onCartChange$ = cartService.OnCartChange();
    this.totalInfo$ = cartService.getTotalInfo();
    this.isPlacedOrder = false;
  }

  ngOnInit(): void {
  }

  backCart()
  {
    this.back.emit(true);
    this.cartService.closeCartPage();
  }

  //only the items in check stack can be removed or shopped
  trash()
  {
    for(let item of this.cartService.checkItemStack)
    {
      this.cartService.removeItemFromCart(item)
      .pipe(
        first(),
        tap(cart=>{
          this.cartService.removeItemFromCheckStack(item);
        })
      )
      .subscribe(cart=>{
        if(!cart){
          alert("remove item failed!");
        }
      });
    }
  }
  shop()
  {
    //after call api, finished shop
    var orderItems: OrderItem[] = this.cartService.checkItemStack.map(_=>{
      return {
        itemid: _.item._id,
        amount: _.quantity,
        fromSharePost: null
      };
    });

    var dto: OrderPostDTO = {
      buyerid: this.buyerService.getBuyerId(),
      orderItems: orderItems
    }

    this.orderService.placeOrder(dto)
    .pipe(
      first(),
      catchError(err=>{
        if(err instanceof HttpErrorResponse && err.status==400){
          alert("商品存貨不足！");
          return throwError(()=>err)
        }else{
          return throwError(()=>err);
        }
      })
    )
    .subscribe(order=>{
      this.isPlacedOrder = true;
    });
  }

  routeToProfile()
  {
    location.reload();
    //this.router.navigate(['/search']);
  }
}
