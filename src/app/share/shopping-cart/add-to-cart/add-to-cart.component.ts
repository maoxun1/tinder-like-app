import { first } from 'rxjs';
import { NumButtonComponent } from './../num-button/num-button.component';
import { ProductPost } from './../../../model/interface/ProductPost';
import { CartItem } from './../../../model/interface/CartItem';
import { ShoppingCartService } from 'src/app/service/shopping-cart-service/shopping-cart.service';
import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-add-to-cart',
  templateUrl: './add-to-cart.component.html',
  styleUrls: ['./add-to-cart.component.css']
})
export class AddToCartComponent implements OnInit {
  @Input('post') post?: ProductPost;
  @ViewChild('numBtn') numBtn?: ElementRef<NumButtonComponent>;

  constructor(
    private cartService: ShoppingCartService
  ) {

  }

  ngOnInit(): void {

  }

  back()
  {
    this.cartService.closeAddToCartPage();
  }

  addToCart()
  {
    if(this.numBtn){
      let quntity = (this.numBtn as any as NumButtonComponent).itemNumber;

      if(this.post){
        if(this.cartService.isItemExist(this.post, this.cartService.buyingItemStack)){
          alert("The item has already been in cart!");
          return;
        }

        let cart = this.cartService.addItemToCart(<CartItem>{
          item: this.post,
          quantity: quntity
        })
        .pipe(first())
        .subscribe(cart=>{
          if(cart){
            alert("The item is added to cart!");
          }
        });
      }
    }
  }

  ngOnDestroy(): void {
    this.back();
  }
}
