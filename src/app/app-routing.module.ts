import { CombinedGuard } from './guards/combined.guard';
import { InitRoleStateGuard } from './auth/guards/init-role-state.guard';
import { ResetPasswordComponent } from './auth/components/reset-password/reset-password/reset-password.component';
import { ResetPasswordGuard } from './auth/guards/reset-password.guard';
import { VerifyCodeComponent } from './auth/components/reset-password/verify-code/verify-code.component';
import { EnterEmailComponent } from './auth/components/reset-password/enter-email/enter-email.component';
import { CheckEmailComponent } from './auth/components/check-email/check-email.component';
import { RegisterComponent } from './auth/components/register/register.component';
import { RouteAuthGuard } from './auth/guards/route-auth.guard';
import { AuthGuard } from './auth/guards/auth.guard';
import { LoginComponent } from './auth/components/login/login.component';
import { AppComponent } from './app.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    canActivate: [CombinedGuard],
    data:{
      guards: [InitRoleStateGuard]
    },
    children:[
      {path: '', redirectTo: 'login', pathMatch: 'full'},
      {
        path: 'login',
        component: LoginComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'main',
        loadChildren: () => import('./main/main.module').then(m => m.MainModule),
        canActivate: [RouteAuthGuard],
        canLoad: [RouteAuthGuard]
      },
      {
        path: 'register',
        component: RegisterComponent
      },
      {
        path: 'check-email',
        component: CheckEmailComponent
      },
      {
        path: 'enter-email',
        component: EnterEmailComponent
      },
      {
        path: 'verify-code',
        component: VerifyCodeComponent,
        canActivate: [ResetPasswordGuard]
      },
      {
        path: 'reset-password',
        component: ResetPasswordComponent,
        canActivate: [ResetPasswordGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})], //use hash, so that server can know component/module routing
  exports: [RouterModule]
})
export class AppRoutingModule { }
