import { Injectable, Injector } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { lastValueFrom, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CombinedGuard implements CanActivate {
  constructor(private injector: Injector){}

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean|UrlTree> {
    const guards = route.data["guards"] || [];
    for(let guard of guards){
      const instatnce: CanActivate = this.injector.get<any>(guard);
      let result = instatnce.canActivate(route, state);

      //Depending on the route result, we may need to await upon it in different ways.
      if(result instanceof Promise) {
        result = await result;
      }

      if(result instanceof Observable) {
        result = await lastValueFrom(result);
      }

      if (result === false || result instanceof UrlTree) {
        return result;
      }
    }
    return true;
  }

}
