import { Pipe, PipeTransform } from '@angular/core';
import {formatDate} from '@angular/common';

@Pipe({
  name: 'commentTime'
})
export class CommentTimePipe implements PipeTransform {

  transform(value: string): string {
    /*
      <1min -> s
      1min<= <60min -> m
      1h<= <24h -> h
      1d<= <7d -> d
      7d<= <365d -> w
      365d<= <3y -> y
      3y<= < ~~  -> date
    */

    let date = new Date(value);
    let now = new Date();

    //in the database, the date faster by 5min 18s
    let dateTime = date.getTime();
    if(dateTime>now.getTime()){
      dateTime = now.getTime()-1000;
    }

    let diff = Math.floor((now.getTime()-dateTime)/1000);

    if(diff<60)
    {
      return diff+"s";
    }

    diff=Math.floor(diff/60);
    if(1<=diff && diff<60)
    {
      return diff+"m";
    }

    diff=Math.floor(diff/60);
    if(1<=diff && diff<24)
    {
      return diff+"h";
    }

    diff=Math.floor(diff/24);
    if(1<=diff && diff<7)
    {
      return diff+"d";
    }

    if(7<=diff && diff<365)
    {
      return Math.floor(diff/7)+"w";
    }

    diff=Math.floor(diff/365);
    if(1<=diff && diff<3)
    {
      return diff+"y";
    }

    return formatDate(value, 'yyyy/MM/dd', '');
  }
}
