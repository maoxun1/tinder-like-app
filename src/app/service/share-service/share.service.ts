import { TypeResponse } from './../../model/interface/Response';
import { LikedSharePost } from './../../model/interface/LikedSharePost';
import { PostService } from 'src/app/service/post-service/post.service';
import { ProductPost } from 'src/app/model/interface/ProductPost';
import { SharePost } from './../../model/interface/SharePost';
import { Observable, retry, tap } from 'rxjs';
import { environment } from './../../../environments/environment';
import { BoughtItem } from './../../model/DTO/BoughtItem';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GetItemShareDTO, SharePicDTO, SharePostDTO, SharePostGetDTO, SharePostReturnDto, ShareVideoDTO } from 'src/app/model/DTO/SharePost';

@Injectable({
  providedIn: 'root'
})
export class ShareService {
  private _selectedBoughtItem?: BoughtItem;
  private _selectedPicsAndVideos: File[]

  public get selectedBoughtItem() { return this._selectedBoughtItem; }
  public get selectedPicsAndVideos() { return this._selectedPicsAndVideos; }
  public set selectedPicsAndVideos(val: File[]) { this._selectedPicsAndVideos = val; }

  baseURI: string;

  constructor(
    private http: HttpClient
  ) {
    this.baseURI = `${environment.domain}/${environment.baseRoute.share}`;
    this._selectedPicsAndVideos = [];
  }

  deletePost(id: string): Observable<{msg: string}>
  {
    var uri = `${this.baseURI}/sharePost/${id}`;
    return this.http.delete<{msg: string}>(uri);
  }
  patchPost(id: string, name: string, content: string): Observable<SharePost>
  {
    var uri = `${this.baseURI}/sharePost/${id}`;
    return this.http.patch<SharePost>(uri, {
      name: name,
      content: content
    });
  }

  getLike(postid: string): Observable<TypeResponse<LikedSharePost[]|null>>
  {
    var uri = `${this.baseURI}/like/${postid}`;
    return this.http.get<TypeResponse<LikedSharePost[]|null>>(uri);
  }
  like(buyerid: string, postid: string): Observable<SharePost>
  {
    var uri = `${this.baseURI}/like`;
    return this.http.patch<SharePost>(uri, {
      buyerid: buyerid,
      postid: postid
    });
  }
  unlike(buyerid: string, postid: string): Observable<SharePost>
  {
    var uri = `${this.baseURI}/unlike`;
    return this.http.patch<SharePost>(uri, {
      buyerid: buyerid,
      postid: postid
    });
  }

  selectVideoFromFiles(): File | null
  {
    var videos: File[] = [];
    for(let file of this._selectedPicsAndVideos){
      if(this.isVideo(file)){
        videos.push(file);
        break;
      }
    }
    return videos.length>=1 ? videos[0] : null;
  }

  selectPicsFromFiles(): File[]
  {
    var pics: File[] = [];
    for(let file of this._selectedPicsAndVideos){
      if(this.isImage(file)){
        pics.push(file);
      }
    }
    return pics;
  }

  isImage(file: File): boolean
  {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    if(file.type.indexOf('image')> -1){
      return true;
    }

    return false;
  }
  isVideo(file: File): boolean
  {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    if(file.type.indexOf('video')> -1){
      return true;
    }

    return false;
  }

  getSpecificSharePost(id: string): Observable<TypeResponse<SharePost>>
  {
    var uri = `${this.baseURI}/sharePost/${id}`;
    return this.http.get<TypeResponse<SharePost>>(uri);
  }

  getSharePostsOfBuyer(dto: SharePostGetDTO): Observable<SharePost[]>
  {
    var uri = `${this.baseURI}/${dto.buyerid}?skip=${dto.skip}&limit=${dto.limit}`;
    return this.http.get<SharePost[]>(uri);
  }

  getSharePostsOfProductPost(dto: GetItemShareDTO): Observable<SharePost[]>
  {
    var uri = `${this.baseURI}/relatedItem/${dto.itemid}?skip=${dto.skip}&limit=${dto.limit}`;
    return this.http.get<SharePost[]>(uri);
  }

  selectBoughtItem(item: BoughtItem): void
  {
    this._selectedBoughtItem = item;
  }

  sharePost(dto: SharePostDTO): Observable<SharePost>
  {
    var uri = `${this.baseURI}/sharePost`;
    return this.http.post<SharePost>(uri, dto);
  }

  sharePostPics(dto: SharePicDTO): Observable<SharePost>
  {
    var uri = `${this.baseURI}/sharePostPics`;
    var formData = new FormData();

    formData.append('buyerid', dto.buyerid);
    formData.append('postid', dto.postid);
    for(let pic of dto.sharePics){
      formData.append('sharePics', pic); // share the same name is OK
    }

    return this.http.post<SharePost>(uri, formData);
  }

  sharePostVideos(dto: ShareVideoDTO): Observable<SharePost>
  {
    var uri = `${this.baseURI}/sharePostVideoes`;
    var formData = new FormData();
    formData.append('buyerid', dto.buyerid);
    formData.append('postid', dto.postid);
    formData.append('sharePics', JSON.stringify(dto.shareVideos)); //strinify a file array to a string

    return this.http.post<SharePost>(uri, formData);
  }
}
