import { BoughtItem } from './../../model/DTO/BoughtItem';
import { ProductPost } from './../../model/interface/ProductPost';
import { OrderGetDTO } from '../../model/DTO/OrderGet';
import { Observable } from 'rxjs';
import { Order } from './../../model/interface/Order';
import { OrderPostDTO } from './../../model/DTO/OrderPost';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { OrderReturnDTO } from 'src/app/model/DTO/OrderReturnDTO';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  baseURI: string

  constructor(
    private http: HttpClient
  ) {
    this.baseURI = `${environment.domain}/${environment.baseRoute.order}`;
  }

  placeOrder(order: OrderPostDTO): Observable<OrderReturnDTO>
  {
    var uri = this.baseURI;
    return this.http.post<OrderReturnDTO>(uri, order);
  }

  getOrders(dto: OrderGetDTO): Observable<Order[]>
  {
    let uri = `${this.baseURI}/${dto.buyerid}?skip=${dto.skip}&limit=${dto.limit}`;
    return this.http.get<Order[]>(uri);
  }

  getBoughtItems(dto: OrderGetDTO): Observable<BoughtItem[]>
  {
    let uri = `${this.baseURI}/boughtItems/${dto.buyerid}?skip=${dto.skip}&limit=${dto.limit}`;
    return this.http.get<BoughtItem[]>(uri);
  }
}
