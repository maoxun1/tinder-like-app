import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FollowPostDto, FollowReturnDto, GetFollowersDto, GetFollowingsDto } from 'src/app/model/DTO/Follow';
import { Follow } from 'src/app/model/interface/Follow';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FollowService {
  baseURI: string;

  constructor(
    private http: HttpClient,
  ) {
    this.baseURI = `${environment.domain}/${environment.baseRoute.follow}`;
  }

  follow(dto: FollowPostDto): Observable<Follow>
  {
    //follower追蹤, following被追蹤
    var uri = this.baseURI;
    return this.http.post<Follow>(uri, dto); //回傳被追蹤者
  }

  unfollow(dto: FollowPostDto): Observable<Follow>
  {
    var uri = `${this.baseURI}/${dto.followType}/${dto.followerid}/${dto.followingid}`;
    return this.http.delete<Follow>(uri);
  }

  getFollowings(dto: GetFollowingsDto): Observable<FollowReturnDto[]>
  {
    var uri = `${this.baseURI}/followings/${dto.userid}/${dto.skip}/${dto.limit}`;
    return this.http.get<FollowReturnDto[]>(uri);
  }

  getFollowers(dto: GetFollowersDto): Observable<FollowReturnDto[]>
  {
    var uri = `${this.baseURI}/followers/${dto.userid}/${dto.skip}/${dto.limit}`;
    return this.http.get<FollowReturnDto[]>(uri);
  }

  isFollow(followerid: string, followingid: string): Observable<boolean>
  {
    var uri = `${this.baseURI}/isFollow/${followerid}/${followingid}`;
    return this.http.get<boolean>(uri);
  }
}
