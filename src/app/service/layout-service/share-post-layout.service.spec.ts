import { TestBed } from '@angular/core/testing';

import { SharePostLayoutService } from './share-post-layout.service';

describe('SharePostLayoutService', () => {
  let service: SharePostLayoutService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SharePostLayoutService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
