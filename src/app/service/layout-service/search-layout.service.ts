import { ProductPost } from 'src/app/model/interface/ProductPost';
import { Buyer } from 'src/app/model/interface/Buyer';
import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Shop } from 'src/app/model/interface/Shop';

export enum searchResultStatus {
  bigpost = "bigpost",
  buyer = "buyer",
  shop = "shop",
  none = "none"
};

export interface searchResult {
  status: searchResultStatus,
  data: Buyer | ProductPost | Shop | null
}

@Injectable({
  providedIn: 'root'
})
export class SearchLayoutService {
  searchPageSubject$: BehaviorSubject<searchResult>;

  constructor() {
    this.searchPageSubject$ = new BehaviorSubject<searchResult>({
      status: searchResultStatus.none,
      data: null
    });
  }

  setSearchResult(result: searchResult): void
  {
    this.searchPageSubject$.next({
      status: result.status,
      data: result.data
    });
  }

  getSearchPage(): Observable<searchResult>
  {
    return this.searchPageSubject$.asObservable();
  }
}
