import { TestBed } from '@angular/core/testing';

import { ShopModeLayoutService } from './shop-mode-layout.service';

describe('ShopModeLayoutService', () => {
  let service: ShopModeLayoutService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShopModeLayoutService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
