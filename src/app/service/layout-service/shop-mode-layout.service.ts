import { ProductPost } from './../../model/interface/ProductPost';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

export enum shopEditLayoutState{
  shopProfile = "shopProfile",
  porductList = "productList",
  editProduct = "editProduct"
}

@Injectable({
  providedIn: 'root'
})
export class ShopModeLayoutService {
  private _shopEditLayoutSubject$: BehaviorSubject<shopEditLayoutState|{state: shopEditLayoutState, post: ProductPost}>

  constructor() {
    this._shopEditLayoutSubject$ = new BehaviorSubject<shopEditLayoutState|{state: shopEditLayoutState, post: ProductPost}>(shopEditLayoutState.shopProfile);
  }

  onShopEditStateChange(): Observable<shopEditLayoutState|{state: shopEditLayoutState, post: ProductPost}>
  {
    return this._shopEditLayoutSubject$.asObservable();
  }
  setShopEditState(state: shopEditLayoutState, post?: ProductPost): void
  {
    if(state===shopEditLayoutState.editProduct && post){
      this._shopEditLayoutSubject$.next({state: state, post: post});
    }else{
      this._shopEditLayoutSubject$.next(state);
    }
  }

}
