import { SharePost } from './../../model/interface/SharePost';
import { BehaviorSubject, Observable, share, Subject } from 'rxjs';
import { Injectable } from '@angular/core';

// the order is mater!
export enum sharingPostState
{
  selectBoughtItems="selectBoughtItems",
  postEditing="postEditing",
  checkPostPhotos="checkPostPhotos"
}

@Injectable({
  providedIn: 'root'
})
export class SharePostLayoutService {
  private _currentSharingState: keyof typeof sharingPostState;
  private _sharingStateSubject$: BehaviorSubject<keyof typeof sharingPostState>;

  private _openSharePostSubject$: Subject<string>;

  private _openEditSharePostSubject$: Subject<SharePost|null>;

  public get currentSharingState() { return this._currentSharingState; }

  constructor() {
    // init state
    this._currentSharingState = 'selectBoughtItems';
    this._sharingStateSubject$ = new BehaviorSubject<keyof typeof sharingPostState>(this._currentSharingState);
    this._openSharePostSubject$ = new Subject<string>();
    this._openEditSharePostSubject$ = new Subject<SharePost|null>();
  }

  onOpenEditSharePostPanel(): Observable<SharePost|null>
  {
    return this._openEditSharePostSubject$.asObservable();
  }
  openEditSharePost(post: SharePost)
  {
    this._openEditSharePostSubject$.next(post);
  }
  closeEditSharePost()
  {
    this._openEditSharePostSubject$.next(null);
  }


  openSharePost(shareid: string)
  {
    this._openSharePostSubject$.next(shareid);
  }
  onOpenSharePost(): Observable<string>
  {
    return this._openSharePostSubject$.asObservable();
  }

  onSharingStateChange(): Observable<keyof typeof sharingPostState>
  {
    return this._sharingStateSubject$.asObservable();
  }
  setSharingState(state: keyof typeof sharingPostState): void
  {
    this._currentSharingState = state;
    this._sharingStateSubject$.next(this._currentSharingState);
  }

  getStateIndex(state: keyof typeof sharingPostState): number
  {
    return Object.keys(sharingPostState).indexOf(state);
  }

  getPreviousPageState(currentState: keyof typeof sharingPostState): keyof typeof sharingPostState
  {
    var index = Object.keys(sharingPostState).indexOf(currentState)-1;
    return Object.values(sharingPostState)[index];
  }

  getNextPageState(currentState: keyof typeof sharingPostState): keyof typeof sharingPostState
  {
    var index = Object.keys(sharingPostState).indexOf(currentState)+1;
    return Object.values(sharingPostState)[index];
  }
}
