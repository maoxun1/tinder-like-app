import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { Injectable, Type } from '@angular/core';

export type followPageType = "following" | "follower";
export interface FollowPageFactor
{
  isOpen: boolean,
  type: followPageType
}

@Injectable({
  providedIn: 'root'
})
export class ProfileLayoutService {
  private optionSubject: Subject<string>;
  private followPageSubject$: BehaviorSubject<FollowPageFactor>;

  constructor() {
    this.optionSubject = new Subject<string>();
    this.followPageSubject$ = new BehaviorSubject<FollowPageFactor>({
      isOpen: false,
      type: "following"
    });
  }

  setOption(type: string): void
  {
    this.optionSubject.next(type);
  }

  getOption$(): Observable<string>
  {
    return this.optionSubject.asObservable();
  }

  onFollowPageChange(): Observable<FollowPageFactor>
  {
    //reset
    return this.followPageSubject$.asObservable();
  }

  openFollow(type: followPageType): void
  {
    this.followPageSubject$.next({
      isOpen: true,
      type: type
    });
  }

  closeFollow(type: followPageType): void
  {
    this.followPageSubject$.next({
      isOpen: false,
      type: type
    });
  }
}
