import { BuyerService } from './../buyer-service/buyer.service';
import { ItemToCartDTO, ReturnCartDTO, PatchAmountReqDTO } from './../../model/DTO/Cart';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, first, of, map } from 'rxjs';
import { ProductPost } from 'src/app/model/interface/ProductPost';
import { Injectable } from '@angular/core';
import { CartItem } from 'src/app/model/interface/CartItem';

export interface totalInfo
{
  nums: number,
  prices: number
}

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  private _buyingItemStack: CartItem[]; //0->last => latest->oldest
  private _checkedItemStack: CartItem[];
  private _totalCheckedItemNum: number;
  private _totalCheckedPrice: number;

  private cartSubject$: BehaviorSubject<CartItem[]>;
  private cartPageSubject$: BehaviorSubject<boolean>;
  private addToCartPageSubject$: BehaviorSubject<boolean>;
  private totalInfoSubject$: BehaviorSubject<totalInfo>;

  public get buyingItemStack() { return this._buyingItemStack; } //readonly
  public get checkItemStack() { return this._checkedItemStack; }
  public get totalItemNum() { return this._totalCheckedItemNum; }
  public get totalPrice() { return this._totalCheckedPrice; }

  baseURI: string;

  constructor(
    private http: HttpClient,
    private buyerService: BuyerService
  ) {
    this.baseURI = `${environment.domain}/${environment.baseRoute.shopCart}`;
    this._buyingItemStack = []; //need to call api
    this.initBuyingStack();
    this._checkedItemStack = []; // every time is empty
    this._totalCheckedItemNum = 0;
    this._totalCheckedPrice = 0;
    this.cartSubject$ = new BehaviorSubject<CartItem[]>(this._buyingItemStack);
    this.cartPageSubject$ = new BehaviorSubject<boolean>(false);
    this.addToCartPageSubject$ = new BehaviorSubject<boolean>(false);
    this.totalInfoSubject$ = new BehaviorSubject<totalInfo>({
      nums: this._totalCheckedItemNum,
      prices: this._totalCheckedPrice
    });
  }

  initBuyingStack(): void
  {
    this.doGetCart()
    .pipe(first())
    .subscribe(dto=>{
      //sort by created time
      dto.cartDetails.sort((d1,d2) => {
        return new Date(d2.createdAt).getTime() - new Date(d1.createdAt).getTime()
      });
      this._buyingItemStack = dto.cartDetails.map(detail=>{
        return {
          item: detail.item,
          quantity: detail.amount
        }
      });

      this.updateCart();
    });
  }

  //call api part
  doAddItemToCart(itemToCartDTO: ItemToCartDTO): Observable<ReturnCartDTO>
  {
    var uri = `${this.baseURI}`;
    return this.http.post<ReturnCartDTO>(uri, itemToCartDTO);
  }
  doGetCart(): Observable<ReturnCartDTO>
  {
    var uri = `${this.baseURI}`;
    return this.http.get<ReturnCartDTO>(uri);
  }
  doPatchAmount(patchDTO: PatchAmountReqDTO): Observable<ReturnCartDTO>
  {
    var uri = `${this.baseURI}/amount`;
    return this.http.patch<ReturnCartDTO>(uri, patchDTO);
  }
  doRemoveItemFromCart(itemid: string)
  {
    var uri = `${this.baseURI}/${itemid}`;
    return this.http.delete<ReturnCartDTO>(uri);
  }

  //only front-end cart part
  addItemToCart(item: CartItem): Observable<ReturnCartDTO|null>
  {
    //先確定加了沒
    if(this.isItemExist(item.item, this._buyingItemStack)){
      return of(null);
    }

    this._buyingItemStack.unshift(item);
    this.updateCart();

    return this.doAddItemToCart({
      buyerid: this.buyerService.getBuyerId(),
      itemid: item.item._id,
      fromSharePost: null,
      amount: item.quantity,
      cost: item.item.price*item.quantity
    }).pipe(first())
  }

  removeItemFromCart(item: CartItem): Observable<ReturnCartDTO|null>
  {
    if(!this.isItemExist(item.item, this._buyingItemStack)){
      return of(null);
    }

    this._buyingItemStack = this._buyingItemStack.filter(_=>_.item._id!=item.item._id);
    this.updateCart();

    return this.doRemoveItemFromCart(item.item._id).pipe(first());
  }

  addItemToCheckStack(item: CartItem): boolean
  {
    if(this.isItemExist(item.item, this._checkedItemStack)){
      return false;
    }

    this._checkedItemStack.unshift(item);
    this.updateCart();
    return true;
  }

  removeItemFromCheckStack(item: CartItem): boolean
  {
    if(!this.isItemExist(item.item, this._checkedItemStack)){
      return false;
    }

    this._checkedItemStack = this._checkedItemStack.filter(_=>_.item._id!=item.item._id);
    this.updateCart();
    return true;
  }

  updateItemQuantityInCart(item: CartItem, newNum: number): Observable<ReturnCartDTO|null>
  {
    if(this.isItemExist(item.item, this._buyingItemStack)){
      let target = this._buyingItemStack.find(_=>_.item._id===item.item._id);
      target!.quantity = newNum;
      this.updateCart();

      return this.doPatchAmount({
        buyerid: this.buyerService.getBuyerId(),
        itemid: item.item._id,
        amount: newNum
      }).pipe(first());
    }
    return of(null);
  }

  getTotalInfo(): Observable<totalInfo>
  {
    return this.totalInfoSubject$.asObservable();
  }

  OnCartChange(): Observable<CartItem[]>
  {
    return this.cartSubject$.asObservable();
  }

  isItemExist(post: ProductPost, stack: CartItem[]): boolean
  {
    for(let item of stack)
    {
      if(item.item._id === post._id){
        return true;
      }
    }
    return false;
  }

  //to let main page know, to display full page
  openedCartPage(): void
  {
    this.cartPageSubject$.next(true);
  }
  closeCartPage(): void
  {
    this.cartPageSubject$.next(false);
  }
  OnCartPageChange(): Observable<boolean>
  {
    return this.cartPageSubject$.asObservable();
  }

  //open add to cart page
  openAddToCartPage(): void
  {
    this.addToCartPageSubject$.next(true);
  }
  closeAddToCartPage(): void
  {
    this.addToCartPageSubject$.next(false);
  }
  OnAddToCartPageChange(): Observable<boolean>
  {
    return this.addToCartPageSubject$.asObservable();
  }

  private updateCart(): void
  {
    let num = 0;
    let price = 0;
    for(let item of this._checkedItemStack)
    {
      num++;
      price += item.item.price * item.quantity;
    }
    this._totalCheckedItemNum = num;
    this._totalCheckedPrice = price;

    this.cartSubject$.next(this._buyingItemStack);
    this.totalInfoSubject$.next({
      nums: this._totalCheckedItemNum,
      prices: this._totalCheckedPrice
    });
  }
}
