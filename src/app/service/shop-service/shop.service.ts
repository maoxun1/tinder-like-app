import { Response, TypeResponse } from './../../model/interface/Response';
import { Observable, first, catchError, throwError, tap, of, mapTo, map, switchMap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable, Type } from '@angular/core';
import { Shop } from 'src/app/model/interface/Shop';
import { environment } from 'src/environments/environment';
import { PatchShopProfileDTO, PatchShopProfilePicDTO } from 'src/app/model/DTO/Shop';

@Injectable({
  providedIn: 'root'
})
export class ShopService {
  baseURI: string;

  shopId: string = "";
  SHOP_ID: string = "SHOP_ID";

  constructor(
    private http: HttpClient
  ) {
    this.baseURI = `${environment.domain}/${environment.baseRoute.shop}`;
  }

  getShopByName(name: string): Observable<Shop[]>
  {
    var uri = `${this.baseURI}/shop/${name}`;
    return this.http.get<TypeResponse<Shop[]>>(uri)
    .pipe(
      switchMap(res=>{
        return of(res.data);
      })
    );
  }

  patchShopProfile(dto: PatchShopProfileDTO): Observable<TypeResponse<Shop>|Response>
  {
    var uri = `${this.baseURI}`;
    return this.http.patch<TypeResponse<Shop>|Response>(uri, dto);
  }
  patchShopProfilePic(dto: PatchShopProfilePicDTO): Observable<TypeResponse<Shop>|Response>
  {
    var uri = `${this.baseURI}/profilePic`;
    var formData = new FormData();
    formData.append('shopid', dto.shopid);
    formData.append('profilePic', dto.profilePic);

    return this.http.patch<TypeResponse<Shop>|Response>(uri, formData);
  }

  getShop(id?: string)
  {
    var uri = "";
    if(id){
      uri = this.baseURI+`/${id}`;
    }else{
      uri = this.baseURI+`/${this.getShopId()}`;
    }
    return this.http.get<Shop>(uri);
  }

  isShopNameTaken(name: string): Observable<TypeResponse<{isTaken: true}>>
  {
    var uri = `${this.baseURI}/name/${name}`;
    return this.http.get<TypeResponse<{isTaken: true}>>(uri);
  }

  getShopId(): string
  {
    return localStorage.getItem(this.SHOP_ID) as string;
  }

  doGetShopId(): Observable<TypeResponse<{shopid: string}>>
  {
    var uri = `${this.baseURI}/id`;
    return this.http.get<TypeResponse<{shopid: string}>>(uri);
  }

  setShopId(): Observable<boolean>
  {
    if(!this.isHasShopId()){
      return this.doGetShopId()
      .pipe(
        first(),
        catchError(err=>{
          return throwError(()=>err)
        }),
        map(res=>{
          if(res.status){
            this.shopId = res.data.shopid;
            this.storeShopId();
            return true;
          }else{
            return false;
          }
        })
      )
    }

    return of(false);
  }

  storeShopId()
  {
    localStorage.setItem(this.SHOP_ID, this.shopId);
  }

  isHasShopId(): boolean
  {
    return !!localStorage.getItem(this.SHOP_ID);
  }
}
