import { ShopService } from './../shop-service/shop.service';
import { BuyerService } from 'src/app/service/buyer-service/buyer.service';
import { PostService } from 'src/app/service/post-service/post.service';
import { BehaviorSubject, forkJoin, Observable, of, switchMap, mergeMap, concatAll, mergeAll, map } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Buyer } from 'src/app/model/interface/Buyer';
import { ProductPost } from 'src/app/model/interface/ProductPost';
import { Shop } from 'src/app/model/interface/Shop';

export interface searchFactor{
  key: string,
  type: "search" | "getSearch" | "account"
}

// Shop之後再處理
export interface searchResult{
  posts: ProductPost[],
  accounts: (Buyer|Shop)[]
}

export type searchState = "ready" | "autofill" | "result";

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  private _searchState: searchState;
  private _keyword: string;

  private _search$: Observable<searchResult>;
  private _searchSubject$: BehaviorSubject<searchFactor>;

  private _searchStateSubject$: BehaviorSubject<searchState>;

  private _keywordResetSubject$: BehaviorSubject<string>;

  public get searchState() { return this._searchState }
  public get keyword() { return this._keyword }
  public set keyword(val: string) { this._keyword=val }

  constructor(
    private http: HttpClient,
    private postService: PostService,
    private buyerService: BuyerService,
    private shopService: ShopService
  ) {
    this._keyword = "";
    this._keywordResetSubject$ = new BehaviorSubject<string>(this._keyword);

    this._searchState = "ready"; //initial state
    this._searchStateSubject$ = new BehaviorSubject<searchState>(this._searchState);

    //main search Observable
    this._searchSubject$ = new BehaviorSubject<searchFactor>({key: "", type: "search"});
    this._search$ = this._searchSubject$.pipe(
      switchMap((factor: searchFactor)=>{
        if(factor.key!="" && factor.key.trim()!=""){
          this._keyword = factor.key //set keyword again
          this.notifyKeywordReset();
          if(factor.type=="search" && factor.key.trim()!=""){
            return forkJoin({
              accounts: of([]),
              posts: this.postService.searchPost(factor.key, false)
            });
          }else if(factor.type=="getSearch"){
            //specific name search
            return forkJoin({
              accounts: of([]),
              posts: this.postService.getSearchPost(factor.key)
            });
          } else if(factor.type=="account"){
            //search account, only by keyword
            return forkJoin({
              accounts: forkJoin([
                this.buyerService.getBuyersByName(factor.key),
                this.shopService.getShopByName(factor.key)
              ]).pipe(
                map(res=>res.flat())
              ),
              posts: of([])
            });
          }else{
            return of({accounts: [], posts: []});
          }
        }else{
          return of({accounts: [], posts: []});
        }
      })
    )

  }

  //state management
  onSearchStateChange(): Observable<searchState>
  {
    return this._searchStateSubject$.asObservable();
  }
  setSearchState(state: searchState): void
  {
    this._searchStateSubject$.next(state);
  }

  //main search function
  getSearchResult(): Observable<searchResult>
  {
    return this._search$;
  }
  setSearchFactor(factor: searchFactor): void
  {
    this._searchSubject$.next(factor);
  }

  //change keyword event
  onKeywordReset(): Observable<string>
  {
    return this._keywordResetSubject$.asObservable();
  }
  private notifyKeywordReset()
  {
    this._keywordResetSubject$.next(this._keyword);
  }
}
