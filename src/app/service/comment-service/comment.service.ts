import { SharePostCommentLike } from './../../model/interface/SharePostCommentLike';
import { SharePostCommentDisplayDTO, SharePostCommentDTO, SharePostCommentLikeDTO } from './../../model/DTO/SharePostComment';
import { SharePostComment } from './../../model/interface/SharePostComment';
import { ProductPostCommentLike } from './../../model/interface/ProductPostCommentLike';
import { ProductCommentLikeDTO, ProductPostCommentDisplayDTO } from './../../model/DTO/ProductComment';
import { BuyerService } from './../buyer-service/buyer.service';
import { Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ProductCommentDTO } from 'src/app/model/DTO/ProductComment';
import { ProductPostComment } from 'src/app/model/interface/ProductPostComment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  baseURI: string;

  constructor(
    private http: HttpClient,
    private buyerService: BuyerService
  ) {
    this.baseURI = `${environment.domain}/${environment.baseRoute.comment}`;
  }

  getSharePostComments(id: string): Observable<SharePostCommentDisplayDTO[]>
  {
    var uri = `${this.baseURI}/sharePost/${id}`;
    return this.http.get<any>(uri).pipe(
      switchMap(coms=>{
        return of(coms["comments"] as SharePostCommentDisplayDTO[]);
      })
    );
  }
  sendSharePostComment(postid: string, comment: string): Observable<SharePostComment>
  {
    var uri: string = `${this.baseURI}/sharePost`;
    let commentorId: string = this.buyerService.getBuyerId();
    const dto: SharePostCommentDTO = {
      commentorId: commentorId,
      postId: postid,
      comment: comment
    };
    return this.http.post<SharePostComment>(uri, dto);
  }
  likeSharePostComment(dto: SharePostCommentLikeDTO): Observable<SharePostCommentLike>
  {
    var uri = `${this.baseURI}/sharePost/like`;
    return this.http.post<SharePostCommentLike>(uri, dto);
  }

  getProductComments(id: string): Observable<ProductPostCommentDisplayDTO[]>
  {
    let uri = `${this.baseURI}/product/${id}`;
    return this.http.get<any>(uri)
    .pipe(
      switchMap(coms=>{
        return of(coms["comments"] as ProductPostCommentDisplayDTO[]);
      })
    );
  }
  sendComment(itemid: string, comment: string): Observable<ProductPostComment>
  {
    let commentorId: string = this.buyerService.getBuyerId();
    const dto: ProductCommentDTO = {
      commentorId: commentorId,
      itemId: itemid,
      comment: comment,
      isShowed: true
    };

    return this.http.post<ProductPostComment>(`${this.baseURI}/product`, dto);
  }
  getProductComment(commentId: string): Observable<ProductPostComment>
  {
    return this.http.get<ProductPostComment>(`${this.baseURI}/comment/${commentId}`);
  }
  likeProductComment(productCommentLikeDTO: ProductCommentLikeDTO): Observable<ProductPostCommentLike>
  {
    return this.http.post<ProductPostCommentLike>(`${this.baseURI}/product/like`, productCommentLikeDTO);
  }
}
