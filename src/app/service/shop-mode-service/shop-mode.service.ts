import { ModifyProductDTO } from './../../model/DTO/ModifyProductDTO';
import { ProductPicDTO } from './../../model/DTO/ProductPicDTO';
import { ProductPostDTO } from './../../model/DTO/ProductPostDTO';
import { Label } from './../../model/interface/Partial';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Subject, Observable, tap } from 'rxjs';
import { Injectable, Type } from '@angular/core';
import { ProductPost } from 'src/app/model/interface/ProductPost';
import { TypeResponse } from 'src/app/model/interface/Response';

export interface displayLabels
{
  type: "label" | "feLabel",
  id: number,
  name: string
}

@Injectable({
  providedIn: 'root'
})
export class ShopModeService {
  private _currentRoute: string = "";
  private _previousRoute: string = "";

  baseURI: string;

  cacheSelectedLabels: displayLabels[] = [];

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    this.baseURI = `${environment.domain}/${environment.baseRoute.shop}`;
    router.events.subscribe((event: any)=>{
      if(event instanceof NavigationEnd){
        this._currentRoute = event.url;
      }
    });
  }

  patchPost(dto: ModifyProductDTO): Observable<ProductPost>
  {
    var uri = `${this.baseURI}/productPost`;
    return this.http.patch<ProductPost>(uri, dto).pipe(
      tap(()=>this.cacheSelectedLabels=[])
    );
  }

  deletePost(post: ProductPost): Observable<{msg: string}>
  {
    var uri = `${this.baseURI}/productPost/${post._id}`;
    return this.http.delete<{msg: string}>(uri);
  }

  getProductPostsOfShop(shopid: string): Observable<TypeResponse<ProductPost[]>>
  {
    var uri = `${this.baseURI}/productPosts/${shopid}`;
    return this.http.get<TypeResponse<ProductPost[]>>(uri);
  }

  patchProductPostPics(dto: ProductPicDTO): Observable<ProductPost>
  {
    var uri = `${this.baseURI}/productPost/pics`;
    var formData = new FormData();
    formData.append('shopid', dto.shopid);
    formData.append('postid', dto.postid);
    for(let file of dto.productPics)
    {
      formData.append('productPics', file);
    }

    return this.http.patch<ProductPost>(uri, formData);
  }

  createProductPost(dto: ProductPostDTO): Observable<ProductPost>
  {
    var uri = `${this.baseURI}/productPost`;
    return this.http.post<ProductPost>(uri, dto).pipe(
      tap(() => this.cacheSelectedLabels = [])
    );
  }

  //get labels from shopee (by search keyword)
  getLabels(keyword: string): Observable<{ labels: Label[], fe_labels: Label[] }>
  {
    var uri = `${this.baseURI}/labels/${keyword}`;
    return this.http.get<{
      labels: Label[],
      fe_labels: Label[]
    }>(uri);
  }

  parsePostLabelsToDisplayLabels(post: ProductPost): displayLabels[]
  {
    var display: displayLabels[] = [];

    post.labels.forEach(l => {
      display.push({
        type: "label",
        id: l.labelid,
        name: l.display_name
      });
    });

    post.feLabels.forEach(l => {
      display.push({
        type: "feLabel",
        id: l.labelid,
        name: l.display_name
      });
    });

    return display;
  }

  openCreatePage(): void
  {
    this._previousRoute = this._currentRoute;
    this.router.navigate(['main', 'shop-mode', 'shop-create-post']);
  }
  closerCreatePage(): void
  {
    this.router.navigate(this._previousRoute.split('/'));
    this._previousRoute = this._currentRoute;
  }

}
