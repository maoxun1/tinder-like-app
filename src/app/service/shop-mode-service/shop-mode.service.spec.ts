import { TestBed } from '@angular/core/testing';

import { ShopModeService } from './shop-mode.service';

describe('ShopModeService', () => {
  let service: ShopModeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShopModeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
