import { Observable, map, of, tap } from 'rxjs';
import { ShopService } from './../service/shop-service/shop.service';

import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';

export class CustomValidator
{
  static sellerNameTakenAsync(shopService: ShopService): AsyncValidatorFn{
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      return shopService.isShopNameTaken(control.value)
      .pipe(
        map(res => res.data.isTaken ? {isTaken: true} : null )
      );
    };
  }
}
