import { first } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ResetPasswordService } from 'src/app/auth/services/reset-password.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  checkPasswords: ValidatorFn = (group: AbstractControl):  ValidationErrors | null => {
    let newPass = group.get('newPassword')?.value;
    let confirmPass = group.get('confirmPassword')?.value
    return newPass === confirmPass ? null : { notSame: true }
  }

  passwordForm: FormGroup = new FormGroup({
    newPassword: new FormControl('', [Validators.required, Validators.minLength(6), Validators.pattern('[a-z0-9A-Z]+')]),
    confirmPassword: new FormControl('', [Validators.required]),
  }, {validators: this.checkPasswords});

  public get newPassword() { return this.passwordForm.get("newPassword"); }
  public get confirmPassword() { return this.passwordForm.get("confirmPassword"); }

  isSeeNewPassword: boolean = false;
  isSeeConfirmPassword: boolean = false;

  email: string;

  constructor(
    private resetService: ResetPasswordService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.email = route.snapshot.queryParams['email'];
  }

  ngOnInit(): void {
  }

  seePassword(type: string)
  {
    switch(type)
    {
      case "new":
        this.isSeeNewPassword = !this.isSeeNewPassword;
        break;
      case "confirm":
        this.isSeeConfirmPassword = !this.isSeeConfirmPassword;
        break;
    }
  }

  sendResetPassword()
  {
    const { email, code } = this.route.snapshot.queryParams;
    if(this.newPassword){
      this.resetService.resetPassword(this.newPassword.value, code, email)
      .pipe(first())
      .subscribe(res=>{
        if(!res.status){
          alert(res.message);
        }else{
          alert("Change password successfully! Please signin again!");
          this.router.navigate(['/login']);
        }
      });
    }
  }

}
