import { Router } from '@angular/router';
import { first } from 'rxjs';
import { ResetPasswordService } from './../../../services/reset-password.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-enter-email',
  templateUrl: './enter-email.component.html',
  styleUrls: ['./enter-email.component.css']
})
export class EnterEmailComponent implements OnInit {
  emailForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email])
  });

  public get email() { return this.emailForm.get("email"); }

  constructor(
    private resetService: ResetPasswordService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  sendEmail()
  {
    if(this.email){
      this.resetService.sendVerificationEmail(this.email.value)
      .pipe(first())
      .subscribe(res=>{
        if(!res.status){
          alert(res.message);
        }
      });

      //先換路由, 等信寄
      if(this.email){
        this.router.navigate(
          ['/verify-code'],
          { queryParams: { email: this.email.value} }
        );
      }
    }
  }

}
