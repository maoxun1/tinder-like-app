import { first } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { ResetPasswordService } from 'src/app/auth/services/reset-password.service';

@Component({
  selector: 'app-verify-code',
  templateUrl: './verify-code.component.html',
  styleUrls: ['./verify-code.component.css']
})
export class VerifyCodeComponent implements OnInit {
  codeForm: FormGroup = new FormGroup({
    code: new FormControl('', [Validators.required, Validators.min(1000), Validators.max(9999)])
  });

  public get code() { return this.codeForm.get("code"); }

  resendCounter: number = 0;
  readonly resendLimit: number = 5;

  constructor(
    private resetService: ResetPasswordService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
  }

  sendVerificationCode()
  {
    if(this.code){
      const { email } = this.route.snapshot.queryParams;
      this.resetService.verifyCode(email, this.code.value)
      .pipe(first())
      .subscribe(res=>{
        if(!res.status){
          alert(res.message);
        }else{
          if(this.code){
            this.router.navigate(['/reset-password'], {queryParams: {
              email: email,
              code: this.code.value
            }});
          }
        }
      });
    }
  }

  resendEmail()
  {
    if(this.resendCounter <= this.resendLimit){
      const { email } = this.route.snapshot.queryParams;
      this.resetService.sendVerificationEmail(email)
      .pipe(first())
      .subscribe(res=>{
        if(res.status){
          alert("Email has been sent!");
          this.resendCounter++;
        }
      });
    }else{
      alert("Resend too many times! Please enter email again");
      this.router.navigate(['/enter-email']);
    }
  }
}
