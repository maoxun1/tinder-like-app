import { SellerRegisterDTO } from './../../model/DTO/SellerRegisterDTO';
import { TypeResponse } from './../../model/interface/Response';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Response } from 'src/app/model/interface/Response';

@Injectable({
  providedIn: 'root'
})
export class AuthSellerService {
  baseURI: string

  constructor(
    private http: HttpClient
  ) {
    this.baseURI = `${environment.domain}`;
  }

  isRegisteredSeller(buyerid: string): Observable<TypeResponse<{isRegistered: boolean}>>
  {
    var uri = `${this.baseURI}/isRegisteredSeller/${buyerid}`;
    return this.http.get<TypeResponse<{
      isRegistered: boolean
    }>>(uri);
  }

  registerSeller(dto: SellerRegisterDTO): Observable<Response>
  {
    var uri = `${this.baseURI}/seller-register`;
    return this.http.post<Response>(uri, dto);
  }
}
