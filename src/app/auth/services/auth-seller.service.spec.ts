import { TestBed } from '@angular/core/testing';

import { AuthSellerService } from './auth-seller.service';

describe('AuthSellerService', () => {
  let service: AuthSellerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthSellerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
