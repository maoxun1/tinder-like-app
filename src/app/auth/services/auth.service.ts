import { ShopService } from './../../service/shop-service/shop.service';
import { TypeResponse } from './../../model/interface/Response';
import { AuthSellerService } from './auth-seller.service';
import { Buyer } from 'src/app/model/interface/Buyer';
import { BuyerService } from './../../service/buyer-service/buyer.service';
import { catchError, mapTo, Observable, of, tap, first, BehaviorSubject, switchMap, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Tokens } from '../models/token';
import { environment } from 'src/environments/environment';
import { SocialAuthService, SocialUser } from 'angularx-social-login';
import { FacebookLogInDto } from 'src/app/model/DTO/SocialLogin';
import { Response } from 'src/app/model/interface/Response';
import { Shop } from 'src/app/model/interface/Shop';

export interface accountState{
  buyer: boolean,
  shop: boolean
}
export enum accountType {
  buyer = 'buyer',
  shop = 'shop'
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  //easy to use intelligence
  private readonly ACCESS_TOKEN = "ACCESS_TOKEN";
  private readonly REFRESH_TOKEN = "REFRESH_TOKEN";
  private readonly LOGGED_USER = "LOGGED_USER";
  private readonly BUYER_ID="BUYER_ID";
  private readonly SHOP_ID="SHOP_ID";

  // init by init-state-guard on routing to main component based on token
  private _accountState: accountState = {
    buyer: true,
    shop: false
  };
  accountStateSubject$: BehaviorSubject<accountState>;

  public get accountState() { return this._accountState; }
  public set accountState(val: accountState) { this._accountState = val; }

  constructor(
    private http: HttpClient,
    private buyerService: BuyerService,
    private shopService: ShopService,
    private authSellerService: AuthSellerService
  ) {
    this.accountStateSubject$ = new BehaviorSubject<accountState>(this._accountState);
  }

  onAccountStateChange(): Observable<accountState>
  {
    return this.accountStateSubject$.asObservable();
  }

  switchAccount(): void
  {
    /**
     * Flow:
     * 1. check buyer to seller or seller to buyer
     * (buyer to seller)
     * 1. check if registered seller
     * 2. -No-> throw error
     * 3. -yes-> call switch account api, get email and new token
     * 4. clear new token and store shop info
     * 5. component switch to shop mode
     */
    var uri = `${environment.domain}/switch-account`;
    if(this._accountState.buyer){
      // if has shop registeration, then switch account and change account state
      this.authSellerService.isRegisteredSeller(this.buyerService.getBuyerId())
      .pipe(
        first(),
        switchMap(res=>{
          if(res.data.isRegistered){
            return this.http.post<TypeResponse<{
              email: string,
              newAccessToken: string,
              newRefreshToken: string
            }>>(uri, {to: accountType.shop});
          }else{
            alert(res.message);
            return throwError(()=>{err: "This account has not registered shop yet."})
          }
        })
      )
      .subscribe(res=>{
          if(res.data.newAccessToken && res.data.newRefreshToken){
            this.doLogoutUser();
            this.doLoginUser(res.data.email, {
              accessToken: res.data.newAccessToken,
              refreshToken: res.data.newRefreshToken
            });

            // set shop id
            this.shopService.setShopId()
            .pipe(first())
            .subscribe(suc=>{
              if(suc){
                // change state
                this._accountState.buyer = false;
                this._accountState.shop = true;
                this.accountStateSubject$.next(this._accountState);
              }
            });
          }
      })
    }else{
      this.http.post<TypeResponse<{
        email: string,
        newAccessToken: string,
        newRefreshToken: string
      }>>(uri, {to: accountType.buyer})
      .subscribe(res=>{
        this.doLogoutUser();
        this.doLoginUser(res.data.email, {
          accessToken: res.data.newAccessToken,
          refreshToken: res.data.newRefreshToken
        });

        // set buyer id
        this.buyerService.setBuyerId()
        .pipe(first())
        .subscribe(suc=>{
          if(suc){
            // change state
            this._accountState.buyer = true;
            this._accountState.shop = false;
            this.accountStateSubject$.next(this._accountState);
          }
        });

      });
    }
  }

  getRoleByToken(): Observable<TypeResponse<{role: string}>>
  {
    var uri = `${environment.domain}/tokenRole`;
    return this.http.get<TypeResponse<{role: string}>>(uri);
  }
  getRoleByIDExist(): keyof typeof accountType | ''
  {
    var bid = localStorage.getItem(this.BUYER_ID);
    var sid = localStorage.getItem(this.SHOP_ID);
    return bid ? 'buyer' : sid ? 'shop' : '';
  }

  loginWithFacebook(user: SocialUser): Observable<Response>
  {
    var uri = `${environment.domain}/login/facebook`;

    // call the backend api to stor info from facebook to database
    const dto: FacebookLogInDto = {
      email: user.email,
      name: user.name,
      profilePic: user.response.picture.data.url
    };

    return this.http.post<Response>(uri, dto).pipe(
      tap(res=>{
        if(res.status){
          let tokens: Tokens = res.data as Tokens;
          this.doLoginUser(dto.email, tokens);
        }
      })
    );
  }

  login(user:{email: string, password: string}): Observable<boolean>
  {
    //使用environment 點參數
    return this.http.post<Tokens>(`${environment.domain}/login`, user)
    .pipe(
      tap(tokens => this.doLoginUser(user.email, tokens)), //side effect
      mapTo(true), //map a each observable to a given value
      catchError(err => {
        console.log(err);
        alert((<Response>err.error).message); //alert popout
        return of(false); //return an observable of false
      })
    )
  }

  logout()
  {
   this.doLogoutUser();
  }

  isLoggedIn()
  {
    //turn to boolean type and reverse
    //undefined -> true -> false
    return !!this.getAccessToken();
  }

  refreshToken()
  {
    //index route may change
    return this.http.post<any>(`${environment.domain}/refresh`,
      {
        email: this.getLoggedUser(),
        refreshToken: this.getRefreshToken(),
        id: this.buyerService.getBuyerId() ? this.buyerService.getBuyerId() : this.shopService.getShopId() //buyer or shop
      }
    )
    .pipe(
      tap((token: any)=>{
        this.storeAccessToken(token);
      })
    );
  }

  public getAccessToken()
  {
    return localStorage.getItem(this.ACCESS_TOKEN);
  }

  public getRefreshToken()
  {
    return localStorage.getItem(this.REFRESH_TOKEN);
  }

  public getLoggedUser()
  {
    return localStorage.getItem(this.LOGGED_USER);
  }

  private doLoginUser(user: string, tokens: Tokens) {
    // this.loggedUser = username; //can change to Buyer and extract email
    //it will be refreshed when reload page
    this.storeUser(user);
    this.storeTokens(tokens);
  }

  private doLogoutUser()
  {
    // this.loggedUser = "";
    this.removeUser();
    this.removeTokens();
  }

  private storeAccessToken(token: string){
    localStorage.setItem(this.ACCESS_TOKEN, token);
  }

  private storeTokens(tokens: Tokens) {
    localStorage.setItem(this.ACCESS_TOKEN, tokens.accessToken);
    localStorage.setItem(this.REFRESH_TOKEN, tokens.refreshToken);
  }

  private removeTokens() {
    localStorage.removeItem(this.ACCESS_TOKEN);
    localStorage.removeItem(this.REFRESH_TOKEN);
  }

  private storeUser(user: string){
    localStorage.setItem(this.LOGGED_USER, user);
  }

  private removeUser(){
    localStorage.removeItem(this.LOGGED_USER);
    localStorage.removeItem(this.BUYER_ID);
    localStorage.removeItem(this.SHOP_ID);
  }
}
