import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { Response } from '../../model/interface/Response';

@Injectable({
  providedIn: 'root'
})
export class ResetPasswordService {
  /*
    流程：
     	1. 輸入信箱 (前端) example/enter-email (email藉著query parameters傳遞下去給下個page)
      2. 寄送帶有驗證code的信件 (後端)
      3. 輸入驗證碼page (前端) example/verification-code
      4. 輸入完驗證碼後, 後端與資料庫對照, 回傳OK和驗證碼
      5. 前端redirect去reset page(auth guard保護)並戴上query 驗證碼 example/reset?id =test&code=test
      6. 透過router取得query中的驗證碼, 並在reset的時候一併傳給後端 重設密碼時 再驗證一次
  */

  baseURI: string = `${environment.domain}`;

  constructor(private http: HttpClient) { }

  sendVerificationEmail(email: string): Observable<Response>
  {
    var uri = `${this.baseURI}/reset/password/email`;
    return this.http.post<Response>(uri, {email: email});
  }

  verifyCode(email: string, code: string): Observable<Response>
  {
    var uri = `${this.baseURI}/verification/resetPasswordCode?code=${code}&email=${email}`;
    return this.http.get<Response>(uri);
  }

  resetPassword(newPassword: string, code: string, email: string): Observable<Response>
  {
    var uri = `${this.baseURI}/reset/password`;
    return this.http.patch<Response>(uri, {
      newPassword: newPassword,
      email: email,
      code: code
    });
  }
}
