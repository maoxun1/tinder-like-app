import { AppRoutingModule } from './../app-routing.module';
import { RegisterComponent } from './components/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { TokenInterceptor } from './token.interceptor';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './guards/auth.guard';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { CheckEmailComponent } from './components/check-email/check-email.component';
import { FacebookLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { ResetPasswordComponent } from './components/reset-password/reset-password/reset-password.component';
import { EnterEmailComponent } from './components/reset-password/enter-email/enter-email.component';
import { VerifyCodeComponent } from './components/reset-password/verify-code/verify-code.component';

@NgModule({
  providers:[
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('624247379320724'),
          },
        ],
      } as SocialAuthServiceConfig,
    },
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    CheckEmailComponent,
    ResetPasswordComponent,
    EnterEmailComponent,
    VerifyCodeComponent
  ],
  imports: [
    CommonModule,
    NzIconModule,
    FormsModule,
    NzDropDownModule,
    ReactiveFormsModule,
    SocialLoginModule,
    AppRoutingModule
  ]
})
export class AuthModule { }
