import { TestBed } from '@angular/core/testing';

import { InitRoleStateGuard } from './init-role-state.guard';

describe('InitRoleStateGuard', () => {
  let guard: InitRoleStateGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(InitRoleStateGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
