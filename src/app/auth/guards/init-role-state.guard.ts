import { accountType, AuthService } from './../services/auth.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { catchError, first, Observable, throwError, tap, of, map, mapTo } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InitRoleStateGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router
  ){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if(!this.authService.isLoggedIn()){
      return true; //in appComponent and redirect to login
    }

    // init accountState in authService based on Token
    return this.authService.getRoleByToken()
    .pipe(
      first(),
      catchError((err)=>{
        return throwError(()=>err);
      }),
      tap(res=>{
        if(res.data.role===accountType.buyer){
          this.authService.accountState = {
            buyer: true,
            shop: false
          }
        }else if(res.data.role===accountType.shop){
          this.authService.accountState = {
            buyer: false,
            shop: true
          }
        }
        this.authService.accountStateSubject$.next(this.authService.accountState);
      }),
      map(()=>{
        return true;
      })
    )
  }


}
