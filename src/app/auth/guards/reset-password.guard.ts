import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ResetPasswordGuard implements CanActivate {
  constructor(private router: Router){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree
  {
    var code: string = route.queryParams['code'];
    var email: string = route.queryParams['email'];
    var url = route.url[0].path;

    if(url==="verify-code"){
      if(!email){
        this.router.navigate(['/login']);
      }
    }else if(url==="reset-password"){
      if(!email || !code){
        this.router.navigate(['/login']);
      }
    }else{
      this.router.navigate(['/login']);
    }

    return true;
  }

}
