import { AuthService, accountType } from './../services/auth.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, CanLoad, Route, UrlSegment, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService
  ){ }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let acceptedRoles = route.data['roles'] as ((keyof typeof accountType)[]);

    // redirect stretegy
    if(this.authService.accountState.buyer && !acceptedRoles.includes(accountType.buyer)){
      this.router.navigate(['main', 'tinder-page'])
    }

    if(this.authService.accountState.shop && !acceptedRoles.includes(accountType.shop)){
      this.router.navigate(['main', 'shop-mode']);
    }

    return this.authService.accountState.buyer && acceptedRoles.includes(accountType.buyer) || this.authService.accountState.shop && acceptedRoles.includes(accountType.shop)
  }
}
