export interface Order
{
  buyerid: string,
  boughtFromPost: string,
  shopid: string,
  orderDetails: {
    itemid: string,
    fromSharePost: string|null,
    amount: number,
    totalCost: number
  }[],
  orderStatus: "備貨中" | "賣家已出貨" | "運送中" | "已完成訂單",
  payingStatus: "已付款" | "尚未付款"
}
