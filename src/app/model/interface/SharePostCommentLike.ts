export interface SharePostCommentLike
{
  commentId: string,
  userId: string
}
