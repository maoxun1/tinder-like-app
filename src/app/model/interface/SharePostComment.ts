export interface SharePostComment
{
  _id: string,
  commentorId: string,
  postId: string,
  comment: string,
  likeCount: number,
  isShowed: number
}
