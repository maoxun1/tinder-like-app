export interface SharePost
{
  _id: string,
  orderid: string,
  itemid: string,
  buyerid: string,
  state: "private" | "public",
  name: string,
  content: string,
  images: string[],
  videoes: string[],
  likeCount: number,
  createdAt: string
}
