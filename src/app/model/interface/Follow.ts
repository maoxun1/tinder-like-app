export interface Follow
{
  follower: string,
  following: string,
  followType: followType //0 -> follow buyer, 1 -> follow shop
}

export enum followType {
  buyer=0,
  shop=1
}
