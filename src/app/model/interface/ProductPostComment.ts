export interface ProductPostComment
{
  _id: string,
  commentorId: string,
  itemId: string,
  comment: string,
  isShowed: boolean,
  likeCount: number,
  createdAt?: string
}
