export interface Response
{
  status: boolean;
  code: number;
  message: string;
  data?: any;
}

export interface TypeResponse<T>
{
  status: boolean;
  code: number;
  message: string;
  data: T;
}
