export interface ProductPostCommentReply
{
  commentId: string,
  userId: string,
  content: string,
  isShowed: number
}
