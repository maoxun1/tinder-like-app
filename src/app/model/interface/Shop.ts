import { BuyerFollow, ShopFollow } from "./Partial"

export interface Shop
{
  _id: string,
  buyerid: string,
  createdAt: string,
  email: string,
  followerCount: number,
  followingCount: number,
  likeCount: number,
  profilePic: string,
  public: boolean,
  rating: {
    ratingBad: number,
    ratingGood: number,
    ratingNormal: number,
    ratingStar: number
  }
  role: string,
  selfIntro: string,
  shopAccount: string,
  sp_shopid: string,
  birthday: string,
  updatedAt: string
}
