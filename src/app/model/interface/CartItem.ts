import { ProductPost } from "./ProductPost";

export interface CartItem
{
  item: ProductPost,
  quantity: number
}
