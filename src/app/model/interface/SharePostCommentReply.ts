export interface SharePostCommentReply
{
  commentId: string,
  userId: string,
  content: string,
  isShowed: number
}
