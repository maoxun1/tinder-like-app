import { ProductPost } from './../interface/ProductPost';
export interface OrderReturnDTO
{
  cartid: string,
  buyerid: string,
  orderDetails: {
    item: ProductPost
  }[]
}
