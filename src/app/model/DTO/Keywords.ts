export interface KeywordsGetDTO
{
  userId: string,
  keywords: Keyword[]
}

export interface Keyword
{
  _id: string,
  word: string,
  searchDate: string
}
