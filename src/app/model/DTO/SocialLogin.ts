interface FacebookLogInDto
{
  email: string,
  name: string,
  profilePic: string
}

export { FacebookLogInDto };
