export interface ProductPicDTO
{
  shopid: string,
  postid: string,
  productPics: File[]
}
