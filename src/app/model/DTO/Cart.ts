import { ProductPost } from './../interface/ProductPost';
export interface ItemToCartDTO
{
  buyerid: string,
  itemid: string,
  fromSharePost: string | null,
  amount: number,
  cost: number
}

export interface PatchAmountReqDTO
{
  buyerid: string,
  itemid: string,
  amount: number
}

export interface ReturnCartDTO
{
  cartid: string,
  buyerid: string,
  cartDetails: CartDetail[]
}

export interface CartDetail
{
  item: ProductPost,
  amount: number,
  image: string,
  createdAt: string
}
