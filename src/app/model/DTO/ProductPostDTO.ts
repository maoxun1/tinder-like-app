import { Label } from './../interface/Partial';
export interface ProductPostDTO
{
  shopAccount: string;
  shopid: string;
  name: string;
  content: string;
  labels: Label[];
  feLabels: Label[];
  price: number;
  stock: number;
  discount?: number;
}
