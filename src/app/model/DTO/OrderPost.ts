export interface OrderPostDTO
{
  buyerid: string,
  orderItems: OrderItem[]
}

export interface OrderItem
{
  itemid: string,
  amount: number,
  fromSharePost: string | null
}


