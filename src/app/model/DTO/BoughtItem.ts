export interface BoughtItem
{
  _id: string,
  orderid: string,
  name: string,
  images: string[],
  price: number,
  amount: number,
  payingStatus: string,
  shippingStatus: string
  createdAt: string
}
