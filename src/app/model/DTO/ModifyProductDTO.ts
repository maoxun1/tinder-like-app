import { Label } from './../interface/Partial';
export interface ModifyProductDTO
{
  postid: string,
  name: string,
  content: string,
  labels: Label[],
  feLabels: Label[],
  price: number,
  stock: number,
  discount: number
}
