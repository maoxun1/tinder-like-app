export interface SharePostCommentDisplayDTO
{
  _id: string,
  commentorId: string,
  postId: string,
  comment: string,
  likeCount: number,
  isShowed: number,

  createdAt: string,
  profilePic: string,
  account: string,
  isLiked: boolean
}

export interface SharePostCommentDTO
{
  commentorId: string,
  postId: string,
  comment: string,
  isShowed?: number
}

export interface SharePostCommentLikeDTO
{
  commentId: string,
  userId: string
}
