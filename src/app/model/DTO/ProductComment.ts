export interface ProductCommentDTO
{
  commentorId: string,
  itemId: string,
  comment: string,
  isShowed: boolean,
  updatedAt?: string
}

export interface ProductCommentLikeDTO
{
  commentId: string,
  userId: string
}

export interface ProductCommentReplyDTO
{
  commentId: string,
  userId: string,
  content: string,
  isShowed: number
}

export interface ProductPostCommentDisplayDTO
{
  _id: string
  commentorId: string,
  itemId: string,
  comment: string,
  isShowed: number
  createdAt: string,
  likeCount: number,
  profilePic: string,
  account: string,
  isLiked: boolean
}
