export interface SellerRegisterDTO
{
  buyerid: string,
  shopAccount: string;
  selfIntro: string
}
