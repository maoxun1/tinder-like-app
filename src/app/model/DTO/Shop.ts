interface PatchShopProfileDTO
{
  id: string,
  name: string,
  status: string,
  birthday: string
}

interface PatchShopProfilePicDTO
{
  shopid: string,
  profilePic: File
}

export { PatchShopProfileDTO, PatchShopProfilePicDTO }
