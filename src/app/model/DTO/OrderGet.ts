export interface OrderGetDTO
{
  buyerid: string,
  skip: number,
  limit: number
}
