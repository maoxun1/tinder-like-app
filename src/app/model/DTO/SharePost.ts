interface SharePostDTO
{
  orderid: string,
  itemid: string,
  buyerid: string,
  state?: string,
  name: string,
  content: string
}

interface SharePostReturnDto
{
  postid: string,
  buyerid: string
}

interface SharePicDTO
{
  buyerid: string,
  postid: string,
  sharePics: File[]
}

interface ShareVideoDTO
{
  buyerid: string,
  postid: string,
  shareVideos: File[]
}

interface SharePostGetDTO
{
  buyerid: string,
  skip: number,
  limit: number
}

interface GetItemShareDTO
{
  itemid: string,
  skip: number,
  limit: number
}

export { SharePostDTO, SharePostReturnDto, SharePicDTO, ShareVideoDTO, SharePostGetDTO, GetItemShareDTO };
