import { Shop } from './../interface/Shop';
import { Buyer } from './../interface/Buyer';
import { Follow, followType } from './../interface/Follow';

interface FollowPostDto
{
  followerid: string,
  followingid: string,
  followType: followType //0表示追蹤買家, 1表示追蹤賣家
}

interface GetFollowersDto
{
  userid: string,
  skip: number,
  limit: number
}

interface GetFollowingsDto
{
  userid: string,
  skip: number,
  limit: number
}

interface FollowReturnDto
{
  followInfo: Follow,
  user: Buyer | Shop
}

export { FollowPostDto, GetFollowersDto, GetFollowingsDto, FollowReturnDto };
