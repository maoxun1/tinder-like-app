import { InitRoleStateGuard } from './../auth/guards/init-role-state.guard';
import { accountType } from './../auth/services/auth.service';
import { RoleGuard } from './../auth/guards/role.guard';
import { MainComponent } from './main.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        redirectTo: 'tinder-page',
        pathMatch: 'full'
      },
      {
        path: 'tinder-page',
        loadChildren: () => import('./tinder-page/tinder-page.module').then(m => m.TinderPageModule),
        canActivate: [RoleGuard],
        data: { roles: [accountType.buyer] }
      },
      {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
        canActivate: [RoleGuard],
        data: { roles: [accountType.buyer] }
      },
      {
        path: 'search',
        loadChildren: () => import('./search/search.module').then(m => m.SearchModule),
        canActivate: [RoleGuard],
        data: { roles: [accountType.buyer] }
      },
      {
        path: 'profile',
        loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule),
        canActivate: [RoleGuard],
        data: { roles: [accountType.buyer] }
      },
      {
        path: 'shop-mode',
        loadChildren: ()=> import('./shop-mode/shop-mode.module').then(m => m.ShopModeModule),
        canActivate: [RoleGuard],
        data: { roles: [accountType.shop] }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
