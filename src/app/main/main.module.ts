import { ShopNavbarComponent } from './../layout/shop-navbar/shop-navbar.component';
import { NavbarComponent } from './../layout/navbar/navbar.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzIconModule } from 'ng-zorro-antd/icon';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';

@NgModule({
  declarations: [
    MainComponent,
    NavbarComponent,
    ShopNavbarComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    NzIconModule
  ]
})
export class MainModule { }
