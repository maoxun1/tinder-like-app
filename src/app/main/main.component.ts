import { Router } from '@angular/router';
import { Subject, takeUntil, first } from 'rxjs';
import { ShoppingCartService } from 'src/app/service/shopping-cart-service/shopping-cart.service';
import { BuyerService } from './../service/buyer-service/buyer.service';
import { AuthService, accountState, accountType } from './../auth/services/auth.service';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy {

  isFullPage: boolean;
  accountState: accountState;
  destroy$: Subject<any>;

  constructor(
    private cartService: ShoppingCartService,
    private authService: AuthService,
    private router: Router
  ) {
    this.isFullPage = false;
    this.destroy$ = new Subject<any>();
    this.accountState = authService.accountState;
  }

  ngOnInit(): void {
    this.cartService.OnCartPageChange()
    .pipe(takeUntil(this.destroy$))
    .subscribe(isOpened=>{
      this.isFullPage = isOpened;
    })

    this.authService.onAccountStateChange()
    .pipe(takeUntil(this.destroy$))
    .subscribe(state=>{
      this.accountState = state;
      if(this.accountState.buyer){
        this.router.navigate(['/']);
      }else if(this.accountState.shop){
        this.router.navigate(['main','shop-mode','shop-home']);
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
