import { switchMap } from 'rxjs';
import { ProductPost } from './../../../../model/interface/ProductPost';
import { Component, OnInit, Output, EventEmitter, Input, Type } from '@angular/core';

enum contentType {
  productInfo = "productInfo",
  comments = "comments",
  sharePosts = "sharePosts",
  shopCart = "shopCart"
}

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css']
})
export class ProductInfoComponent implements OnInit {
  @Output('closeInfo') closeInfo: EventEmitter<any>;
  @Input('post') post?: ProductPost;

  contentType: typeof contentType = contentType;
  currentType: string;
  previousType: string;

  constructor() {
    this.closeInfo = new EventEmitter<any>();
    this.currentType = contentType.productInfo;
    this.previousType = this.currentType;
  }

  ngOnInit(): void {

  }

  switchContent(type: contentType)
  {
    if(type===this.currentType && this.currentType===contentType.shopCart){
      this.currentType = this.previousType;
    }else if(type!=this.currentType){
      this.previousType = this.currentType;
      this.currentType = type;
    }
  }

  back()
  {
    this.closeInfo.emit();
  }
}
