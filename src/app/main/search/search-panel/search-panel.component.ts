import { AuthService } from 'src/app/auth/services/auth.service';
import { Buyer } from './../../../model/interface/Buyer';
import { searchFactor, searchResult, SearchService, searchState } from './../../../service/search-service/search.service';
import { KeywordsGetDTO } from './../../../model/DTO/Keywords';
import { SearchLayoutService, searchResultStatus } from './../../../service/layout-service/search-layout.service';
import { BuyerService } from './../../../service/buyer-service/buyer.service';
import { ProductPost } from 'src/app/model/interface/ProductPost';
import { PostService } from './../../../service/post-service/post.service';
import { Component, Input, OnInit, Output, SimpleChanges, EventEmitter } from '@angular/core';
import { BehaviorSubject, Observable, switchMap, of, takeWhile, Subject, takeUntil, repeatWhen, skip, map, pipe, forkJoin, first, tap } from 'rxjs';
import { Shop } from 'src/app/model/interface/Shop';

@Component({
  selector: 'app-search-panel',
  templateUrl: './search-panel.component.html',
  styleUrls: ['./search-panel.component.css']
})
export class SearchPanelComponent implements OnInit {
  activeBtn: {[key: string]: boolean} = {
    'item': true,
    'account': false
  };

  destroy$: Subject<any>;

  autoFill$: Observable<string[]>;
  autoFillSubject: BehaviorSubject<string>;

  search$: Observable<searchResult>;
  searchState: searchState;

  retriggerHistory$: BehaviorSubject<any>;
  getSearchHistory$: Observable<KeywordsGetDTO>;

  isInstanceOfBuyer(user: Buyer|Shop): user is Buyer
  {
    // if true, will return true and covert user to Buyer
    return user.role==='buyer';
  }
  isInstanceOfShop(user: Buyer|Shop): user is Shop
  {
    return user.role==='shop';
  }

  constructor(
    private postService: PostService,
    private buyerService: BuyerService,
    authService: AuthService,
    private searchLayoutService: SearchLayoutService,
    private searchService: SearchService
  ) {
    this.destroy$ = new Subject<any>();

    this.autoFillSubject = new BehaviorSubject<string>(searchService.keyword); //initial value
    this.autoFill$ = this.autoFillSubject.pipe(
      takeUntil(this.destroy$),
      switchMap((key: string)=>{
        if(key!="" && key.trim()!=""){
          return this.postService.searchPost(key).pipe(
            switchMap(res=>of(res.map(x=>x.name).splice(0,10)))
          );
        }else{
          return of([]);
        }
      })
    );

    //main search function, by searchService
    this.search$ = searchService.getSearchResult();
    this.searchState = 'ready';

    //searchHistory
    this.retriggerHistory$ = new BehaviorSubject<any>(null);
    this.getSearchHistory$ = this.retriggerHistory$
    .pipe(switchMap(dummy=>{
      return postService.getSearchHistory();;
    }))

  }

  ngOnInit(): void {
    this.searchService.onSearchStateChange()
    .pipe(takeUntil(this.destroy$))
    .subscribe(state=>{
      this.searchState = state;
      if(state==='autofill'){
        this.autoFillSubject.next(this.searchService.keyword);
      }else if(state==='result'){
        this.searchService.setSearchFactor({
          key: this.searchService.keyword,
          type: this.activeBtn['item'] ? "search" : "account"
        });
      }
    });
  }

  searchItem(key: string, type: "search"|"getSearch"|"account")
  {
    this.searchService.setSearchState('result');

    this.searchService.setSearchFactor({
      key: key,
      type: this.activeBtn['account'] ? 'account' : type
    });
  }

  changeMode(type: string)
  {
    Object.entries(this.activeBtn).forEach(item=>{
      this.activeBtn[item[0]] = false;
    });

    switch(type)
    {
      case 'item':
        this.activeBtn['item'] = true;
        this.searchService.setSearchFactor({
          key: this.searchService.keyword,
          type: "search"
        });
        break;
      case 'account':
        this.activeBtn['account'] = true;
        this.searchService.setSearchFactor({
          key: this.searchService.keyword,
          type: "account"
        });
        break;
    }
  }

  openPost(post: ProductPost)
  {
    this.searchLayoutService.setSearchResult({
      status: searchResultStatus.bigpost,
      data: post
    });
  }

  openUser(user: Buyer|Shop)
  {
    var status = this.isInstanceOfBuyer(user) ? searchResultStatus.buyer
    : this.isInstanceOfShop(user) ? searchResultStatus.shop : searchResultStatus.none;

    this.searchLayoutService.setSearchResult({
      status: status,
      data: user
    });
  }

  clearHistory(): void
  {
    this.postService.clearHistory()
    .pipe(first())
    .subscribe(res=>{
      this.retriggerHistory$.next(null);
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
