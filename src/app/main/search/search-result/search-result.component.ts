import { ProductPost } from 'src/app/model/interface/ProductPost';
import { BuyerService } from './../../../service/buyer-service/buyer.service';
import { Component, OnInit } from '@angular/core';
import { Observable, Subject, takeUntil } from 'rxjs';
import { Buyer } from 'src/app/model/interface/Buyer';
import { SearchLayoutService, searchResult, searchResultStatus } from 'src/app/service/layout-service/search-layout.service';
import { Shop } from 'src/app/model/interface/Shop';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {
  parseBuyer(val: any): Buyer { return val as Buyer }
  parsePost(val: any): ProductPost {return val as ProductPost}
  parseBuyerOrShop(val: any): Buyer|Shop {return val as (Buyer|Shop)}
  searchResultStatus: typeof searchResultStatus = searchResultStatus
  searchResult$: Observable<searchResult>;
  destroy$: Subject<any>;

  constructor(
    private searchLayoutService: SearchLayoutService,
    private buyerService: BuyerService
  ) {
    this.destroy$ = new Subject<any>();
    this.searchResult$ = searchLayoutService.getSearchPage().pipe(takeUntil(this.destroy$));
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    //can use subject to unsubscribe all observable
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  backBigPost()
  {
    this.searchLayoutService.setSearchResult({
      status: searchResultStatus.none,
      data: null
    })
  }
}
