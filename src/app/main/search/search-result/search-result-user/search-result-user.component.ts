import { ProductPost } from 'src/app/model/interface/ProductPost';
import { SharePost } from './../../../../model/interface/SharePost';
import { AuthService } from './../../../../auth/services/auth.service';
import { ShopService } from './../../../../service/shop-service/shop.service';
import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, first, Observable, of, Subject, switchMap, takeUntil } from 'rxjs';
import { Buyer } from 'src/app/model/interface/Buyer';
import { Shop } from 'src/app/model/interface/Shop';
import { BuyerService } from 'src/app/service/buyer-service/buyer.service';
import { FollowService } from 'src/app/service/follow-service/follow.service';
import { ProfileLayoutService } from 'src/app/service/layout-service/profile-layout.service';
import { SearchLayoutService, searchResultStatus } from 'src/app/service/layout-service/search-layout.service';

@Component({
  selector: 'app-search-result-user',
  templateUrl: './search-result-user.component.html',
  styleUrls: ['./search-result-user.component.css']
})
export class SearchResultUserComponent implements OnInit {
  @Input('user') searchUser?: Buyer | Shop;

  searchResultStatus: typeof searchResultStatus = searchResultStatus

  isFollow$: Observable<boolean>;
  IsFollowRetrigger$: BehaviorSubject<any>;
  isFollowOpen: boolean;
  isOpenBigPosts: boolean;

  bufferBigPosts: {posts: SharePost[] | ProductPost[], init: number} | null;

  destroy$: Subject<any>;

  isInstanceOfBuyer(user: Buyer|Shop): user is Buyer
  {
    // if true, will return true and covert user to Buyer
    return user.role==='buyer';
  }
  isInstanceOfShop(user: Buyer|Shop): user is Shop
  {
    return user.role==='shop';
  }

  parseBuyer(val: any): Buyer { return val as Buyer }
  parseShop(val: any): Shop {return val as Shop}

  constructor(
    private searchLayoutService: SearchLayoutService,
    private buyerService: BuyerService,
    private shopService: ShopService,
    private authService: AuthService,
    private followService: FollowService,
    private profileLayoutService: ProfileLayoutService
  ) {
    this.destroy$ = new Subject<any>();
    this.IsFollowRetrigger$ = new BehaviorSubject<any>(null);
    this.isFollow$ = this.IsFollowRetrigger$.pipe(
      takeUntil(this.destroy$),
      switchMap(dummy=>{
        if(this.searchUser){
          let role = authService.getRoleByIDExist();
          let myid = role=='buyer' ? buyerService.getBuyerId() : role=='shop' ? shopService.getShopId() : '';
          return followService.isFollow(myid, this.searchUser._id);
        }
        return of(false);
      })
    );
    this.isFollowOpen = false;
    this.isOpenBigPosts = false;
    this.bufferBigPosts = null;
  }

  ngOnInit(): void {
    this.profileLayoutService.onFollowPageChange()
    .pipe(takeUntil(this.destroy$))
    .subscribe(factor=>{
      this.isFollowOpen = factor.isOpen;
    });
  }

  ngOnDestroy(): void {
    //can use subject to unsubscribe all observable
    this.destroy$.next(null);
    this.destroy$.complete();
    this.profileLayoutService.closeFollow("following");
  }

  openProductPosts(event: { posts: ProductPost[], init: number })
  {
    this.bufferBigPosts = event;
    this.isOpenBigPosts = true;
  }
  backBigProductPosts()
  {

    this.bufferBigPosts = null;
    this.isOpenBigPosts = false;
  }

  openSharePosts(event: { posts: SharePost[], init: number })
  {
    this.bufferBigPosts = event;
    this.isOpenBigPosts = true;
  }
  backBigSharePosts()
  {

    this.bufferBigPosts = null;
    this.isOpenBigPosts = false;
  }

  back()
  {
    this.searchLayoutService.setSearchResult({
      status: searchResultStatus.none,
      data: null
    });
  }

  getMyID()
  {
    let role = this.authService.getRoleByIDExist();
    let myid = role=='buyer' ? this.buyerService.getBuyerId() : role=='shop' ? this.shopService.getShopId() : '';
    return myid;
  }

  follow()
  {
    if(this.searchUser){
      this.followService.follow({
        followerid: this.getMyID(),
        followingid: this.searchUser._id,
        followType: this.searchUser.role==='buyer' ? 0 : 1
      })
      .pipe(first())
      .subscribe(follow => {
        if(this.searchUser){
          this.searchUser.followerCount+=1; //先加在前端, 刷新頁面
          this.IsFollowRetrigger$.next(null);
        }
      });
    }
  }

  unfollow()
  {
    if(this.searchUser){
      this.followService.unfollow({
        followerid: this.getMyID(),
        followingid: this.searchUser._id,
        followType: this.searchUser.role==='buyer' ? 0 : 1
      })
      .pipe(first())
      .subscribe(follow => {
        if(this.searchUser){
          this.searchUser.followerCount-=1; //先加在前端, 刷新頁面
          this.IsFollowRetrigger$.next(null);
        }
      });
    }
  }
}
