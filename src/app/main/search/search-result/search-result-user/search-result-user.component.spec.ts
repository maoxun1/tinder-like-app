import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchResultUserComponent } from './search-result-user.component';

describe('SearchResultUserComponent', () => {
  let component: SearchResultUserComponent;
  let fixture: ComponentFixture<SearchResultUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchResultUserComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SearchResultUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
