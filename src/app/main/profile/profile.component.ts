import { ShareService } from 'src/app/service/share-service/share.service';
import { SharePost } from './../../model/interface/SharePost';
import { Shop } from './../../model/interface/Shop';
import { accountState, AuthService } from './../../auth/services/auth.service';
import { ShoppingCartService } from 'src/app/service/shopping-cart-service/shopping-cart.service';
import { PostService } from './../../service/post-service/post.service';
import { ProductPost } from 'src/app/model/interface/ProductPost';
import { environment } from 'src/environments/environment';
import { ProfileLayoutService, FollowPageFactor } from './../../service/layout-service/profile-layout.service';
import { Buyer } from './../../model/interface/Buyer';
import { first, Subject, takeUntil, Observable, mergeMap, of, BehaviorSubject, switchMap } from 'rxjs';
import { BuyerService } from './../../service/buyer-service/buyer.service';
import { Router } from '@angular/router';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import SwiperCore, { Pagination, SwiperOptions } from "swiper";

SwiperCore.use([Pagination]);

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  isSpandSettings: boolean;
  isSpandOptionContent: boolean;
  isOpenedCart: boolean;
  isOpenSharePost: boolean;
  isFollowOpen: boolean;
  isOpenBigSharePosts: boolean;

  // need to handle buyer and shop
  // buyer
  buyer$: Observable<Buyer>;
  buyerRetrigger$: BehaviorSubject<any>;

  bufferSharePosts: { posts: SharePost[], init: number } | null;

  destroy$: Subject<any>;

  constructor(
    private _route: Router,
    private buyerService: BuyerService,
    private profileLayoutService: ProfileLayoutService,
    private postService: PostService,
    private cartService: ShoppingCartService,
    private shareService: ShareService
  ){
    this.isSpandSettings = false;
    this.isSpandOptionContent = false;
    this.isOpenedCart = false;
    this.isFollowOpen = false;
    this.isOpenSharePost = false;
    this.isOpenBigSharePosts = false;
    this.destroy$ = new Subject<any>();

    this.buyerRetrigger$ = new BehaviorSubject<any>(null);
    this.buyer$ = this.buyerRetrigger$.pipe(
      takeUntil(this.destroy$),
      switchMap(dummy=>{
        return this.buyerService.getBuyer();
      })
    )

    this.bufferSharePosts = null;
  }

  ngOnInit(): void {
    this.profileLayoutService.getOption$()
    .pipe(takeUntil(this.destroy$))
    .subscribe(type=>{
      if(type != ""){
        this.isSpandOptionContent = true;
      }
    });

    this.profileLayoutService.onFollowPageChange()
    .pipe(takeUntil(this.destroy$))
    .subscribe(factor=>{
      this.isFollowOpen = factor.isOpen;

      //retrigger follow count
      this.buyerRetrigger$.next(null);
    });
  }

  openBigSharePosts(posts: { posts: SharePost[], init: number })
  {
    this.isOpenBigSharePosts = true;
    this.bufferSharePosts = posts;
  }
  backBigSharePosts()
  {
    // retrigger buyer?
    this.buyerRetrigger$.next(null);
    this.isOpenBigSharePosts = false;
    this.bufferSharePosts = null;
  }

  spandSetting()
  {
    this.isSpandSettings = true;
  }

  collapseSettings(event: boolean)
  {
    this.isSpandSettings = event;
  }

  collapseOptionContent(event: boolean)
  {
    this.isSpandOptionContent = event
  }

  counter(n: number)
  {
    return new Array(n);
  }

  openCart()
  {
    this.isOpenedCart = true;
    this.cartService.openedCartPage();
  }
  backCart(event: boolean)
  {
    this.isOpenedCart = !event;
    //in cart component will call close
  }

  openSharePost()
  {
    this.isOpenSharePost = true;
  }
  closeSharePost()
  {
    this.isOpenSharePost = false;
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
    this.profileLayoutService.closeFollow("following");
  }
}
