import { Response } from './../../../../model/interface/Response';
import { catchError, first, throwError, switchMap, of } from 'rxjs';
import { BuyerService } from 'src/app/service/buyer-service/buyer.service';
import { SharePostDTO, SharePicDTO, ShareVideoDTO } from 'src/app/model/DTO/SharePost';
import { BoughtItem } from './../../../../model/DTO/BoughtItem';
import { SharePostLayoutService, sharingPostState } from './../../../../service/layout-service/share-post-layout.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ShareService } from 'src/app/service/share-service/share.service';

@Component({
  selector: 'app-post-editing',
  templateUrl: './post-editing.component.html',
  styleUrls: ['./post-editing.component.css']
})
export class PostEditingComponent implements OnInit {
  postForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.maxLength(60)]),
    caption: new FormControl('', [Validators.required, Validators.maxLength(300)])
  });

  public get name() { return this.postForm.get('name') }
  public get caption() { return this.postForm.get('caption') }

  selectedItem?: BoughtItem;
  isUploading: boolean = false;

  constructor(
    private sharePostLayoutService: SharePostLayoutService,
    private shareService: ShareService,
    private buyerService: BuyerService
  ) {
    this.selectedItem = shareService.selectedBoughtItem;
  }

  ngOnInit(): void {
  }

  checkPhotos()
  {
    this.sharePostLayoutService.setSharingState(sharingPostState.checkPostPhotos);
  }

  upload()
  {
    if(this.postForm.invalid){
      alert("The post form is invalid!");
      return;
    }

    if(!(this.selectedItem && this.name && this.caption)){
      alert("You haven't selected the item you have bought.");
      return;
    }

    this.isUploading = true;
    var dto: SharePostDTO = {
      orderid: this.selectedItem.orderid,
      itemid: this.selectedItem._id,
      buyerid: this.buyerService.getBuyerId(),
      name: this.name.value,
      content: this.caption.value
    };

    this.shareService.sharePost(dto)
    .pipe(
      first(),
      catchError(err=>{
        this.isUploading = false;
        alert((<Response>err.error).message);
        return throwError(()=>err);
      }),
      switchMap(sharePost=>{
        // no shrePost throw error
        if(!sharePost._id){
          alert("Share Post Failed!");
          throw {message: "Share Post Failed!"};
        }

        // no pics
        let pics = this.shareService.selectPicsFromFiles();
        if(pics.length<=0){
          return of(sharePost);
        }

        let picDto: SharePicDTO = {
          buyerid: this.buyerService.getBuyerId(),
          postid: sharePost._id,
          sharePics: this.shareService.selectPicsFromFiles()
        }
        return this.shareService.sharePostPics(picDto);
      }),
      switchMap(sharePost=>{
        let video = this.shareService.selectVideoFromFiles();
        if(!video){
          return of(sharePost);
        }

        let videoDto: ShareVideoDTO={
          buyerid: this.buyerService.getBuyerId(),
          postid: sharePost._id,
          shareVideos: [video]
        };
        return this.shareService.sharePostVideos(videoDto);
      })
    ).subscribe(sharePost=>{
      this.isUploading = false;
      alert("Share Post Successfully!");
      location.reload();
    });

  }
}
