import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckPostPhotosComponent } from './check-post-photos.component';

describe('CheckPostPhotosComponent', () => {
  let component: CheckPostPhotosComponent;
  let fixture: ComponentFixture<CheckPostPhotosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckPostPhotosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CheckPostPhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
