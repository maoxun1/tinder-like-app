import { BoughtItem } from './../../../../model/DTO/BoughtItem';
import { ShareService } from 'src/app/service/share-service/share.service';
import { SharePostLayoutService } from './../../../../service/layout-service/share-post-layout.service';
import { BehaviorSubject, Observable, of, switchMap } from 'rxjs';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import Swiper, { Pagination, SwiperOptions } from 'swiper';

interface PicOrVideo
{
  format: 'image'|'video'|null,
  url: string | ArrayBuffer | null
}

Swiper.use([Pagination])
@Component({
  selector: 'app-check-post-photos',
  templateUrl: './check-post-photos.component.html',
  styleUrls: ['./check-post-photos.component.css']
})
export class CheckPostPhotosComponent implements OnInit {
  @ViewChild('photoNum') photoNum?: ElementRef;
  selectedPicsAndVideos: PicOrVideo[];
  selectedItem?: BoughtItem;

  config: SwiperOptions = {
    pagination: true,
    slidesPerView: 1.2,
    spaceBetween: 10,
    centeredSlides: true
  };

  constructor(
    private sharePostLayoutService: SharePostLayoutService,
    private shareService: ShareService
  ) {
    this.selectedItem = shareService.selectedBoughtItem;
    // init file
    this.selectedPicsAndVideos = [];
    for(let file of shareService.selectedPicsAndVideos){
      let reader = new FileReader();
      reader.readAsDataURL(file);
      let buffer: PicOrVideo = {
        format: null,
        url: null
      };
      if(file.type.indexOf('image')> -1){
        buffer.format = 'image';
      } else if(file.type.indexOf('video')> -1){
        buffer.format = 'video';
      }
      reader.onload = (event) => {
        buffer.url = (<FileReader>event.target).result;
        this.selectedPicsAndVideos.push(buffer);
      }
    }

  }

  ngOnInit(): void {

  }

  onSlideChange($event: [swiper: Swiper])
  {
    (this.photoNum?.nativeElement as HTMLElement).innerText = ($event[0].activeIndex+1).toString()+' / '+(this.selectedPicsAndVideos.length+1);
  }

  ngOnDestroy(): void {
  }
}
