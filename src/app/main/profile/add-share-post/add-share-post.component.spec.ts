import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSharePostComponent } from './add-share-post.component';

describe('AddSharePostComponent', () => {
  let component: AddSharePostComponent;
  let fixture: ComponentFixture<AddSharePostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSharePostComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddSharePostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
