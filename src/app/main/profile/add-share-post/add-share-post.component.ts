import { CheckPostPhotosComponent } from './check-post-photos/check-post-photos.component';
import { PostEditingComponent } from './post-editing/post-editing.component';
import { Subject, takeUntil } from 'rxjs';
import { SharePostLayoutService, sharingPostState } from './../../../service/layout-service/share-post-layout.service';
import { SelectBoughtItemsComponent } from './select-bought-items/select-bought-items.component';
import { Component, OnInit, Output, EventEmitter, ViewChild, ViewContainerRef, Type, OnDestroy, AfterViewInit } from '@angular/core';
import { ShareService } from 'src/app/service/share-service/share.service';

@Component({
  selector: 'app-add-share-post',
  templateUrl: './add-share-post.component.html',
  styleUrls: ['./add-share-post.component.css']
})
export class AddSharePostComponent implements OnInit, AfterViewInit, OnDestroy {
  @Output('backToProfile') backToProfileEmitter: EventEmitter<any>;
  @ViewChild('container', {read: ViewContainerRef}) container!: ViewContainerRef;

  destroy$: Subject<any>;

  typeDict: {[type: string]: Type<any>} = {
    "selectBoughtItems": SelectBoughtItemsComponent,
    "postEditing": PostEditingComponent,
    "checkPostPhotos": CheckPostPhotosComponent
  }
  currentState: keyof typeof sharingPostState='selectBoughtItems';
  sharingPostState: typeof sharingPostState = sharingPostState;

  constructor(
    private shareService: ShareService,
    private sharePostLayoutService: SharePostLayoutService
  ) {
    this.backToProfileEmitter = new EventEmitter<any>();
    this.destroy$ = new Subject<any>();
    sharePostLayoutService.setSharingState('selectBoughtItems');
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.sharePostLayoutService.onSharingStateChange()
    .pipe(takeUntil(this.destroy$))
    .subscribe(state=>{
      this.currentState = state;

      this.container.clear();

      let classType = this.typeDict[state];
      if(classType){
        this.container.createComponent<typeof classType>(classType);
      }
    })
  }

  backToProfile()
  {
    this.backToProfileEmitter.emit();
  }

  next()
  {
    var nextState = this.sharePostLayoutService.getNextPageState(this.currentState);
    if(this.sharePostLayoutService.getStateIndex(nextState)
      > this.sharePostLayoutService.getStateIndex(sharingPostState.selectBoughtItems)
    ){
      if(this.shareService.selectedBoughtItem){
        this.sharePostLayoutService.setSharingState(nextState);
        return;
      }
      alert("Please select one item!");
    }
  }

  back()
  {
    var previousState = this.sharePostLayoutService.getPreviousPageState(this.currentState);
    this.sharePostLayoutService.setSharingState(previousState);
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
    this.shareService.selectedPicsAndVideos = []; //clear cache
  }
}
