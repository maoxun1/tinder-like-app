import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectBoughtItemsComponent } from './select-bought-items.component';

describe('SelectBoughtItemsComponent', () => {
  let component: SelectBoughtItemsComponent;
  let fixture: ComponentFixture<SelectBoughtItemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectBoughtItemsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SelectBoughtItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
