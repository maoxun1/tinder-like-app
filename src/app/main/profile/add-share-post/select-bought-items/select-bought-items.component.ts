import { SharePostLayoutService } from './../../../../service/layout-service/share-post-layout.service';
import { BuyerService } from './../../../../service/buyer-service/buyer.service';
import { BoughtItem } from './../../../../model/DTO/BoughtItem';
import { Observable, first } from 'rxjs';
import { OrderService } from './../../../../service/order-service/order.service';
import { Component, OnInit } from '@angular/core';
import { ShareService } from 'src/app/service/share-service/share.service';

@Component({
  selector: 'app-select-bought-items',
  templateUrl: './select-bought-items.component.html',
  styleUrls: ['./select-bought-items.component.css']
})
export class SelectBoughtItemsComponent implements OnInit {
  boughtItems?: BoughtItem[];
  selectedItem?: BoughtItem;

  constructor(
    private orderService: OrderService,
    private buyerService: BuyerService,
    private shareService: ShareService,
    private sharePostLayoutService: SharePostLayoutService
  ) {
    orderService.getBoughtItems({
      buyerid: buyerService.getBuyerId(),
      skip: 0,
      limit: 0
    })
    .pipe(first())
    .subscribe(items=>{
      this.boughtItems = items.sort((x, y)=>{
        let dateRegex = new RegExp('(上午|下午)+');
        let time1 = (new Date(y.createdAt.replace(dateRegex, ""))).getTime();
        let time2 = (new Date(x.createdAt.replace(dateRegex, ""))).getTime();
        return time1-time2;
      });
      this.selectedItem = this.boughtItems[0];
      this.shareService.selectBoughtItem(this.selectedItem);
    });
  }

  ngOnInit(): void {
  }

  selectItem(item: BoughtItem)
  {
    this.selectedItem = item;
    this.shareService.selectBoughtItem(this.selectedItem);
  }

  onFileSelect(event: Event)
  {
    const element = event.currentTarget as HTMLInputElement;
    let fileList: FileList|null = element.files;

    if(fileList){
      let files = Array.from(fileList);

      // guard, pics<=10 and video<=1
      if(this.isVideosMoreThanOne(files)){
        alert("You cannot select more than one video!");
        event.preventDefault();
        return;
      }

      if(fileList.length>10){
        alert("You cannot choose more than 10 pics!");
        event.preventDefault();
        return;
      }

      this.shareService.selectedPicsAndVideos = files;
    }
  }

  isVideosMoreThanOne(files: File[]): boolean
  {
    var count = 0;
    for(let file of files)
    {
      if(this.shareService.isVideo(file)){
        count++;
        if(count>=2){
          return true;
        }
      }
    }
    return false;
  }
}
