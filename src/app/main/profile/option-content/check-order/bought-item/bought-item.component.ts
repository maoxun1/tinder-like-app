import { BoughtItem } from './../../../../../model/DTO/BoughtItem';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-bought-item',
  templateUrl: './bought-item.component.html',
  styleUrls: ['./bought-item.component.css']
})
export class BoughtItemComponent implements OnInit {
  @Input('bought-item') boughtItem?: BoughtItem;

  constructor() {

  }

  ngOnInit(): void {
    //console.log(this.boughtItem);
  }

}
