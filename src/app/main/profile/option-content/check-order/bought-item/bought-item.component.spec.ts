import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoughtItemComponent } from './bought-item.component';

describe('BoughtItemComponent', () => {
  let component: BoughtItemComponent;
  let fixture: ComponentFixture<BoughtItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoughtItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BoughtItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
