import { BoughtItem } from './../../../../model/DTO/BoughtItem';
import { BuyerService } from './../../../../service/buyer-service/buyer.service';
import { first, Observable, tap, switchMap, of } from 'rxjs';
import { OrderService } from './../../../../service/order-service/order.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-check-order',
  templateUrl: './check-order.component.html',
  styleUrls: ['./check-order.component.css']
})
export class CheckOrderComponent implements OnInit {

  boughtItems$: Observable<BoughtItem[]>;

  constructor(
    private buyerService: BuyerService,
    private orderService: OrderService
  ) {
    //先撈十筆
    this.boughtItems$ = orderService.getBoughtItems({
      buyerid: buyerService.getBuyerId(),
      skip: 0,
      limit: 10
    }).pipe(
      switchMap(items=>{
        let reg = /[\u4e00-\u9fa5]/g; //刪除中文字
        return of(items.sort((x,y) => new Date(y.createdAt.replace(reg, '')).getTime() - new Date(x.createdAt.replace(reg, '')).getTime() ))
      })
    );
  }

  ngOnInit(): void {

  }

}
