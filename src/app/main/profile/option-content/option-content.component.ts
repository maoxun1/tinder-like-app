import { SwitchAccountComponent } from './switch-account/switch-account.component';
import { CheckOrderComponent } from './check-order/check-order.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { Subject, takeUntil } from 'rxjs';
import { ProfileLayoutService } from './../../../service/layout-service/profile-layout.service';
import { Component, Input, OnInit, OnChanges, SimpleChanges, ViewChild, ViewContainerRef, ComponentFactoryResolver, Type, Output, EventEmitter, AfterViewInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-option-content',
  templateUrl: './option-content.component.html',
  styleUrls: ['./option-content.component.css']
})
export class OptionContentComponent implements OnInit, AfterViewInit, OnDestroy{
  @ViewChild('container', {read: ViewContainerRef}) container!: ViewContainerRef;

  @Output('collapse') collapse: EventEmitter<boolean>;

  destroy$: Subject<any>;

  //pass a class as an parameter to the component factory
  typeDict: {[type: string]: Type<any>} = {
    "edit-profile": EditProfileComponent,
    "check-order": CheckOrderComponent,
    "switch-account": SwitchAccountComponent
  }

  //type info icon
  typeInfoDict: {[type: string]: string} = {
    "switch-account": "fa-solid fa-people-arrows-left-right",
    "edit-profile": "fa-solid fa-pen-to-square",
    "QRcode": "fa-solid fa-qrcode",
    "History": "fa-solid fa-clock",
    "check-order": "fa-solid fa-bag-shopping"
  };

  thisType: string;

  constructor(
    private profileLayoutService: ProfileLayoutService
  ) {
    this.destroy$ = new Subject<string>();
    this.collapse = new EventEmitter<boolean>();
    this.thisType = "";
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    //use subscribe to detect settings event
    this.profileLayoutService.getOption$()
    .pipe(takeUntil(this.destroy$))
    .subscribe(type=>{
      this.container.clear();

      this.thisType = type;
      let classType = this.typeDict[type];

      if(classType!=undefined){
        this.thisType = type;
        this.container.createComponent<typeof classType>(classType);
      }
    });
  }

  back(): void
  {
    this.collapse.emit(false);
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
