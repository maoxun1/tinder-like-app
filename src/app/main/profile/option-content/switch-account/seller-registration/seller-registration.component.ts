import { Response } from 'src/app/model/interface/Response';
import { first, catchError, throwError } from 'rxjs';
import { BuyerService } from 'src/app/service/buyer-service/buyer.service';
import { SellerRegisterDTO } from './../../../../../model/DTO/SellerRegisterDTO';
import { AuthSellerService } from './../../../../../auth/services/auth-seller.service';
import { ShopService } from './../../../../../service/shop-service/shop.service';
import { CustomValidator } from './../../../../../custom-validators/custom-validators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-seller-registration',
  templateUrl: './seller-registration.component.html',
  styleUrls: ['./seller-registration.component.css']
})
export class SellerRegistrationComponent implements OnInit {
  @Output('back') backSellerRegistration: EventEmitter<boolean>;
  @Output('switchAccount') switchAccount: EventEmitter<boolean>;

  isNext: boolean = false;
  isDone: boolean = false;
  shopNameInput: string = "";
  selfIntroContent: string = "";
  maxWords: number = 200;

  sellerRegister: FormGroup = new FormGroup({
    shopName: new FormControl(
      '',
      [Validators.required],
      [CustomValidator.sellerNameTakenAsync(this.shopService)] //async custom validator
    ),
    selfIntro: new FormControl(
      '',
      [Validators.required, Validators.maxLength(200)],
      null
    )
  });

  public get shopName() { return this.sellerRegister.get('shopName'); }
  public get selfIntro() { return this.sellerRegister.get('selfIntro'); }

  constructor(
    private shopService: ShopService,
    private buyerService: BuyerService,
    private authSellerService: AuthSellerService
  ) {
    this.backSellerRegistration = new EventEmitter<boolean>();
    this.switchAccount = new EventEmitter<boolean>();
  }

test(){
  console.log(this.shopName)
}

  ngOnInit(): void {
  }

  back()
  {
    this.backSellerRegistration.emit(true);
  }

  next()
  {
    this.isNext = true;
  }

  submit()
  {
    if(this.shopName?.invalid && this.selfIntro?.invalid){
      alert("Invalid seller registration!");
      return;
    }

    var dto: SellerRegisterDTO = {
      buyerid: this.buyerService.getBuyerId(),
      shopAccount: this.shopName?.value,
      selfIntro: this.selfIntro?.value
    };

    this.authSellerService.registerSeller(dto)
    .pipe(
      first(),
      catchError(err=>{
        alert((<Response>err.error).message);
        return throwError(()=>err);
      })
    )
    .subscribe(res=>{
      alert(res.message);
      this.next();
      this.isDone = true;
    })
  }

  done()
  {
    this.switchAccount.emit(this.isDone);
  }
}
