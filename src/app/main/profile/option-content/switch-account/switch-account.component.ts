import { accountState, AuthService } from './../../../../auth/services/auth.service';
import { first, Subject, takeUntil } from 'rxjs';
import { BuyerService } from 'src/app/service/buyer-service/buyer.service';
import { AuthSellerService } from './../../../../auth/services/auth-seller.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-switch-account',
  templateUrl: './switch-account.component.html',
  styleUrls: ['./switch-account.component.css']
})
export class SwitchAccountComponent implements OnInit, OnDestroy {
  sellerCheck: boolean = false;
  buyerCheck: boolean = true;
  accountState: accountState;

  isOpenSellerRegistration: boolean = false;

  destroy$: Subject<any>;

  constructor(
    private buyerService: BuyerService,
    private authSellerService: AuthSellerService,
    private authService: AuthService
  ) {
    this.destroy$ = new Subject<any>();
    this.accountState = authService.accountState;
  }

  ngOnInit(): void {
    this.authService.onAccountStateChange()
    .pipe(
      takeUntil(this.destroy$)
    ).subscribe(state=>{
      this.accountState = state;
      this.updateCheck(this.accountState.buyer?'buyer':'shop');
    });
  }

  // check if has registered seller
  checkSellerRole()
  {
    if(this.authService.accountState.shop){
      if(this.buyerCheck){
        this.updateCheck('shop');
      }
      return;
    }

    //call api to check
    this.authSellerService.isRegisteredSeller(this.buyerService.getBuyerId())
    .pipe(first())
    .subscribe(res=>{
      if(res.data.isRegistered){
        this.updateCheck('shop');
        this.authService.switchAccount();
      }else{
        //if hasn't
        this.isOpenSellerRegistration = true;
      }
    });
  }
  checkBuyerRole()
  {
    if(this.authService.accountState.buyer){
      if(this.sellerCheck){
        this.updateCheck('buyer');
      }
      return;
    }

    this.updateCheck('buyer');
    this.authService.switchAccount();
  }

  backSellerRegistration()
  {
    this.isOpenSellerRegistration = false;
    this.sellerCheck = false;
  }

  handleDoneRegister(isDone: boolean)
  {
    if(isDone){
      this.isOpenSellerRegistration = false;
      this.authService.switchAccount();
    }
  }

  updateCheck(type: "buyer"|"shop")
  {
    if(type==='buyer'){
      this.buyerCheck = true;
      this.sellerCheck = false;
    }else{
      this.buyerCheck = false;
      this.sellerCheck = true;
    }
  }
  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
