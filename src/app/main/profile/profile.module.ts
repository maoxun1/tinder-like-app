import { SellerRegistrationComponent } from './option-content/switch-account/seller-registration/seller-registration.component';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { ReactiveFormsModule } from '@angular/forms';
import { SwiperModule } from 'swiper/angular';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { ShareModule } from 'src/app/share/share.module';
import { SettingsComponent } from './settings/settings.component';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { OptionContentComponent } from './option-content/option-content.component';
import { EditProfileComponent } from './option-content/edit-profile/edit-profile.component';
import { EditStatusComponent } from './option-content/edit-profile/edit-status/edit-status.component';
import { CheckOrderComponent } from './option-content/check-order/check-order.component';
import { BoughtItemComponent } from './option-content/check-order/bought-item/bought-item.component';
import { SwitchAccountComponent } from './option-content/switch-account/switch-account.component';
import { AddSharePostComponent } from './add-share-post/add-share-post.component';
import { SelectBoughtItemsComponent } from './add-share-post/select-bought-items/select-bought-items.component';
import { PostEditingComponent } from './add-share-post/post-editing/post-editing.component';
import { CheckPostPhotosComponent } from './add-share-post/check-post-photos/check-post-photos.component';

@NgModule({
  declarations: [
    ProfileComponent,
    SettingsComponent,
    OptionContentComponent,
    EditProfileComponent,
    EditStatusComponent,
    CheckOrderComponent,
    BoughtItemComponent,
    SwitchAccountComponent,
    SellerRegistrationComponent,
    AddSharePostComponent,
    SelectBoughtItemsComponent,
    PostEditingComponent,
    CheckPostPhotosComponent
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    NzIconModule,
    ShareModule,
    SwiperModule,
    NzModalModule,
    ReactiveFormsModule,
    NzCheckboxModule,
  ],
  exports: [
    ProfileComponent,
    SettingsComponent,
    OptionContentComponent,
    EditStatusComponent,
    SwitchAccountComponent
  ]
})
export class ProfileModule { }
