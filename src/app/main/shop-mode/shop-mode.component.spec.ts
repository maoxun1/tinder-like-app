import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopModeComponent } from './shop-mode.component';

describe('ShopModeComponent', () => {
  let component: ShopModeComponent;
  let fixture: ComponentFixture<ShopModeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShopModeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShopModeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
