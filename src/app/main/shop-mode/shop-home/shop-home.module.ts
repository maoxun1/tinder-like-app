import { HomeModule } from './../../home/home.module';
import { ShopHomeRoutingModule } from './shop-home-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopHomeComponent } from './shop-home.component';



@NgModule({
  declarations: [
    ShopHomeComponent
  ],
  imports: [
    CommonModule,
    ShopHomeRoutingModule,
    HomeModule
  ]
})
export class ShopHomeModule { }
