import { ShopModeComponent } from './shop-mode.component';
import { ShareModule } from './../../share/share.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShopModeRoutingModule } from './shop-mode-routing.module';


@NgModule({
  declarations: [
    ShopModeComponent
  ],
  imports: [
    CommonModule,
    ShopModeRoutingModule,
    ShareModule
  ]
})
export class ShopModeModule { }
