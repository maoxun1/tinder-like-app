import { ShopCreatePostModule } from './../shop-create-post/shop-create-post.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { ShareModule } from './../../../share/share.module';
import { ProfileModule } from './../../profile/profile.module';
import { ShopProfileRoutingModule } from './shop-profile-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopProfileComponent } from './shop-profile.component';
import { ShopSettingsComponent } from './shop-settings/shop-settings.component';
import { ShopOptionContentComponent } from './shop-option-content/shop-option-content.component';
import { ShopEditComponent } from './shop-option-content/shop-edit/shop-edit.component';
import { ProductEditComponent } from './shop-option-content/shop-edit/product-edit/product-edit.component';
import { ProductEditDetailComponent } from './shop-option-content/shop-edit/product-edit/product-edit-detail/product-edit-detail.component';

@NgModule({
  declarations: [
    ShopProfileComponent,
    ShopSettingsComponent,
    ShopOptionContentComponent,
    ShopEditComponent,
    ProductEditComponent,
    ProductEditDetailComponent
  ],
  imports: [
    CommonModule,
    ShopProfileRoutingModule,
    ShareModule,
    ProfileModule,
    NzModalModule,
    ReactiveFormsModule,
    ShopCreatePostModule
  ]
})
export class ShopProfileModule { }
