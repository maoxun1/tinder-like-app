import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthService } from 'src/app/auth/services/auth.service';
import { ProfileLayoutService } from 'src/app/service/layout-service/profile-layout.service';

@Component({
  selector: 'app-shop-settings',
  templateUrl: './shop-settings.component.html',
  styleUrls: ['./shop-settings.component.css']
})
export class ShopSettingsComponent implements OnInit {
  @Output('collapse-settings') collapseSettings: EventEmitter<boolean>;

  isVisible: boolean;

  constructor(
    private authService: AuthService,
    private profileLayoutService: ProfileLayoutService
  ) {
    this.collapseSettings = new EventEmitter<boolean>();
    this.isVisible = false;
  }

  ngOnInit(): void {
  }

  back()
  {
    this.collapseSettings.emit(false);
  }

  logout()
  {
    this.authService.logout();
    location.reload();
  }

  //a big component changes its content when different option activated
  changeOption(type: string): void
  {
    this.profileLayoutService.setOption(type);
  }

}
