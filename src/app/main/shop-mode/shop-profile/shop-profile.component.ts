import { ProductPost } from 'src/app/model/interface/ProductPost';
import { ProfileLayoutService } from './../../../service/layout-service/profile-layout.service';
import { ShopService } from './../../../service/shop-service/shop.service';
import { Observable, BehaviorSubject, takeUntil, Subject, switchMap } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Shop } from 'src/app/model/interface/Shop';

@Component({
  selector: 'app-shop-profile',
  templateUrl: './shop-profile.component.html',
  styleUrls: ['./shop-profile.component.css']
})
export class ShopProfileComponent implements OnInit, OnDestroy {
  destroy$: Subject<any>;

  shop$: Observable<Shop>;
  shopRetrigger$: BehaviorSubject<any>;
  bufferProductPosts?: {posts: ProductPost[], init: number} | null;

  isSpandSettings: boolean = false;
  isSpandOptionContent: boolean = false;
  isOpenBigProductPosts: boolean = false;

  constructor(
    private shopService: ShopService,
    private profileLayoutService: ProfileLayoutService
  ) {
    this.destroy$ = new Subject<any>();

    this.shopRetrigger$ = new BehaviorSubject<any>(null);
    this.shop$ = this.shopRetrigger$
    .pipe(
      takeUntil(this.destroy$),
      switchMap(dummy=>{
        return shopService.getShop();
      })
    );

  }

  ngOnInit(): void {
    this.profileLayoutService.getOption$()
    .pipe(takeUntil(this.destroy$))
    .subscribe(type=>{
      if(type != ""){
        this.isSpandOptionContent = true;
      }
    });
  }

  openBigProductPosts(event: {posts: ProductPost[], init: number})
  {
    this.bufferProductPosts = event;
    this.isOpenBigProductPosts = true;
  }
  backBigProdcutPosts()
  {
    this.shopRetrigger$.next(null);
    this.bufferProductPosts = null;
    this.isOpenBigProductPosts = false;
  }

  spandSetting(){
    this.isSpandSettings = true;
  }
  collapseSettings(event: boolean){
    this.isSpandSettings = false;
  }
  collapseOptionContent(event: boolean){
    this.isSpandOptionContent = false;
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
