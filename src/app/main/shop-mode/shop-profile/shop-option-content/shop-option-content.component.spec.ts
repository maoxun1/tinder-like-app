import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopOptionContentComponent } from './shop-option-content.component';

describe('ShopOptionContentComponent', () => {
  let component: ShopOptionContentComponent;
  let fixture: ComponentFixture<ShopOptionContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShopOptionContentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShopOptionContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
