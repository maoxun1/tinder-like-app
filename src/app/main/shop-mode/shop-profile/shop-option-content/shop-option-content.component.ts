import { ProductPost } from './../../../../model/interface/ProductPost';
import { shopEditLayoutState, ShopModeLayoutService } from './../../../../service/layout-service/shop-mode-layout.service';
import { Component, EventEmitter, OnInit, Output, Type, ViewChild, ViewContainerRef, OnDestroy, ViewRef, ElementRef } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { SwitchAccountComponent } from 'src/app/main/profile/option-content/switch-account/switch-account.component';
import { ProfileLayoutService } from 'src/app/service/layout-service/profile-layout.service';
import { ShopEditComponent } from './shop-edit/shop-edit.component';

@Component({
  selector: 'app-shop-option-content',
  templateUrl: './shop-option-content.component.html',
  styleUrls: ['./shop-option-content.component.css']
})
export class ShopOptionContentComponent implements OnInit, OnDestroy {
  @ViewChild('container', {read: ViewContainerRef}) container!: ViewContainerRef;

  @Output('collapse') collapse: EventEmitter<boolean>;

  destroy$: Subject<any>;

  //pass a class as an parameter to the component factory
  typeDict: {[type: string]: Type<any>} = {
    "switch-account": SwitchAccountComponent,
    "edit": ShopEditComponent,
  }

  //type info icon
  typeInfoDict: {[type: string]: string} = {
    "switch-account": "fa-solid fa-people-arrows-left-right",
    "edit": "fa-solid fa-pen-to-square",
  };

  thisType: string;

  shopEditState: string = shopEditLayoutState.shopProfile;

  constructor(
    private profileLayoutService: ProfileLayoutService,
    private shopModeLayoutService: ShopModeLayoutService
  ) {
    this.destroy$ = new Subject<string>();
    this.collapse = new EventEmitter<boolean>();
    this.thisType = "";
  }

  ngOnInit(): void {
    //use subscribe to detect settings event
    this.profileLayoutService.getOption$()
    .pipe(takeUntil(this.destroy$))
    .subscribe(type=>{
      this.container.clear();

      this.thisType = type;
      let classType = this.typeDict[type];

      if(classType!=undefined){
        this.thisType = type;
        this.container.createComponent<typeof classType>(classType);
      }
    });

    this.shopModeLayoutService.onShopEditStateChange()
    .pipe(takeUntil(this.destroy$))
    .subscribe(state=>{
      if(typeof state === 'string'){
        this.shopEditState = state as shopEditLayoutState;
      }else{
        this.shopEditState = (state as {state: shopEditLayoutState, post: ProductPost}).state;
      }

    });
  }

  back(): void
  {
    if(this.thisType==="switch-account" || (this.thisType==="edit" && this.shopEditState === shopEditLayoutState.shopProfile)){
      this.collapse.emit(false);
    }else if(this.thisType==="edit" && this.shopEditState === shopEditLayoutState.porductList){
      this.shopModeLayoutService.setShopEditState(shopEditLayoutState.shopProfile);
    }else if(this.thisType==="edit" && this.shopEditState === shopEditLayoutState.editProduct){
      this.shopModeLayoutService.setShopEditState(shopEditLayoutState.porductList);
    }

  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
    this.shopModeLayoutService.setShopEditState(shopEditLayoutState.shopProfile);
  }

}
