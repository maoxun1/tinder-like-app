import { ProductPost } from './../../../../../model/interface/ProductPost';
import { shopEditLayoutState, ShopModeLayoutService } from './../../../../../service/layout-service/shop-mode-layout.service';
import { ShopModeService } from 'src/app/service/shop-mode-service/shop-mode.service';
import { PatchShopProfileDTO, PatchShopProfilePicDTO } from 'src/app/model/DTO/Shop';
import { Shop } from 'src/app/model/interface/Shop';
import { ShopService } from './../../../../../service/shop-service/shop.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject, first, forkJoin, mergeMap, Observable, of, catchError, throwError, switchMap, takeUntil, Subject } from 'rxjs';

@Component({
  selector: 'app-shop-edit',
  templateUrl: './shop-edit.component.html',
  styleUrls: ['./shop-edit.component.css']
})
export class ShopEditComponent implements OnInit, OnDestroy {
  editForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    status: new FormControl(''),
    birthday: new FormControl('')
  });

  destroy$: Subject<any>;

  get name() { return this.editForm.get('name'); }
  get status() { return this.editForm.get('status') }
  get birthday() { return this.editForm.get('birthday') }

  shop$: Observable<Shop>;
  shopPic$: Observable<string>;
  refreshPic$: BehaviorSubject<string|null>;
  getPostCount$: Observable<number>;

  isStatusActive: boolean;
  isSaving: boolean;
  isVisible: boolean;
  isOpenProductEdit: boolean;
  isOpenProductEditDetail: boolean;

  popupTitle: string;
  popupContent: string;
  selectedFile?: File;
  previewPicURL: string|null;

  constructor(
    private shopService: ShopService,
    private shopModeService: ShopModeService,
    private shopModeLayoutService: ShopModeLayoutService
  ) {
    this.destroy$ = new Subject<any>();
    this.previewPicURL = null;
    this.refreshPic$ = new BehaviorSubject<string|null>(null);
    this.shop$ = shopService.getShop();
    this.shopPic$ = this.refreshPic$.pipe(
      mergeMap((preview: string|null)=>{
        if(preview){
          return of(preview);
        }else{
          return shopService.getShop().pipe(
            mergeMap(shop=>of(shop.profilePic))
          );
        }
      })
    )

    this.getPostCount$ = shopModeService.getProductPostsOfShop(shopService.getShopId())
    .pipe(
      switchMap(res=>{
        return of(res.data.length);
      })
    );

    this.isStatusActive = false;
    this.isSaving = false;
    this.isVisible = false;
    this.popupTitle = "";
    this.popupContent = "";
    this.isOpenProductEdit = false;
    this.isOpenProductEditDetail = false;
  }

  ngOnInit(): void {
    this.shop$
    .pipe(first())
    .subscribe(shop=>{
      this.editForm.patchValue({
        name: shop.shopAccount,
        status: shop.selfIntro,
        birthday: shop.birthday
      });
    });

    this.shopModeLayoutService.onShopEditStateChange()
    .pipe(takeUntil(this.destroy$))
    .subscribe(state=>{

      if(state===shopEditLayoutState.shopProfile){
        this.isOpenProductEdit = false;
        this.isOpenProductEditDetail = false;
      }else if(state===shopEditLayoutState.porductList){
        this.isOpenProductEditDetail = false
        this.isOpenProductEdit = true;
      }else if((state as {state: shopEditLayoutState, post: ProductPost}).state===shopEditLayoutState.editProduct){
        this.isOpenProductEdit = false;
        this.isOpenProductEditDetail = true;
      }
    });
  }

  editStatus()
  {
    this.isStatusActive = true;
  }

  saveStatus(event: string)
  {
    this.editForm.patchValue({
      status: event
    })
  }

  editProduct()
  {
    this.shopModeLayoutService.setShopEditState(shopEditLayoutState.porductList);
  }

  editProfile()
  {
    let patch: PatchShopProfileDTO = {
      id: this.shopService.getShopId(),
      name: this.editForm.value.name,
      status: this.editForm.value.status,
      birthday: this.editForm.value.birthday,
    }

    let patch$ = null;
    if(this.selectedFile){
      const patchPic: PatchShopProfilePicDTO = {
        shopid: this.shopService.getShopId(),
        profilePic: this.selectedFile
      };
      patch$ = forkJoin({
        profilePicRes: this.shopService.patchShopProfilePic(patchPic),
        shopInfoRes: this.shopService.patchShopProfile(patch)
      })
      .pipe(
        first(),
        catchError(err=>{
          this.createPopUp("Update Profile", "Error occurs while updating profile");
          return throwError(()=>err)
        })
      )
      .subscribe(res=>{
        this.isSaving = true;
        setTimeout(() => {
          if(res.shopInfoRes.status && res.profilePicRes.status){
            this.createPopUp("Update Profile", "Upadte profile successfully!");
          }else{
            this.createPopUp("Update Profile", "Error occurs while updating profile");
          }
          this.isSaving = false;
          location.reload();
        }, 1500);
      });
    }else{
      patch$ = this.shopService.patchShopProfile(patch)
      .pipe(
        first(),
        catchError(err=>{
          this.createPopUp("Update Profile", "Error occurs while updating profile");
          return throwError(()=>err)
        })
      )
      .subscribe(res=>{
        this.isSaving = true;
        setTimeout(() => {
          if(res.status){
            this.createPopUp("Update Profile", "Upadte profile successfully!");
          }else{
            this.createPopUp("Update Profile", "Error occurs while updating profile");
          }
          this.isSaving = false;
          location.reload();
        }, 1500);
      });
    }
  }

  createPopUp(title: string, content: string)
  {
    this.popupTitle = title;
    this.popupContent = content;
    this.isVisible = true;
  }

  onFileSelect(event: Event)
  {
    const element = event.currentTarget as HTMLInputElement;
    let fileList: FileList|null = element.files;
    if(fileList){
      this.selectedFile = fileList[0];
      this.previewPic(this.selectedFile);
    }
  }

  previewPic(file: File)
  {
    const reader = new FileReader();

    //use rxjs to wait
    let readFile$ = new Observable(observer=>{
      reader.onload = e => {
        this.previewPicURL = reader.result as string;
        observer.next(this.previewPicURL);
      }

      reader.readAsDataURL(file);
    })

    readFile$
    .pipe(first())
    .subscribe(preview=>{
      this.refreshPic();
    });

  }

  refreshPic()
  {
    this.refreshPic$.next(this.previewPicURL);
  }

  ngOnDestroy(): void {
    this.refreshPic$.complete();
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
