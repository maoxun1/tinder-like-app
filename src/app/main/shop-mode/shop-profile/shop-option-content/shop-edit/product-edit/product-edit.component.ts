import { ShopModeLayoutService, shopEditLayoutState } from './../../../../../../service/layout-service/shop-mode-layout.service';
import { environment } from 'src/environments/environment';
import { ShopService } from './../../../../../../service/shop-service/shop.service';
import { ProductPost } from 'src/app/model/interface/ProductPost';
import { Observable, of, switchMap, Subject, BehaviorSubject } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ShopModeService } from 'src/app/service/shop-mode-service/shop-mode.service';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {
  getAllPost$: Observable<ProductPost[]>;
  getAllPostRetrigger$: BehaviorSubject<any>;
  imgBase: string = environment.imgBase;

  isModalVisible: boolean = false;
  willDeletePost: ProductPost | null = null;

  constructor(
    private shopService: ShopService,
    private shopModeService: ShopModeService,
    private shopModeLayoutService: ShopModeLayoutService
  ) {
    this.getAllPostRetrigger$ = new BehaviorSubject<any>(null);
    this.getAllPost$ = this.getAllPostRetrigger$.pipe(
      switchMap(dummy=>{
        return shopModeService.getProductPostsOfShop(shopService.getShopId()).pipe(
          switchMap(res=>{
            return of(res.data);
          })
        );
      })
    );
  }

  ngOnInit(): void {
  }

  popModal(post: ProductPost)
  {
    this.isModalVisible = true;
    this.willDeletePost = post;
  }
  deletePost()
  {
    if(this.willDeletePost){
      this.shopModeService.deletePost(this.willDeletePost).subscribe(msg=>{
        this.isModalVisible = false;
        this.willDeletePost = null;
        alert(msg.msg);
        this.getAllPostRetrigger$.next(null);
      });
    }
  }

  editPost(post: ProductPost)
  {
    this.shopModeLayoutService.setShopEditState(shopEditLayoutState.editProduct, post);
  }
}
