import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductEditDetailComponent } from './product-edit-detail.component';

describe('ProductEditDetailComponent', () => {
  let component: ProductEditDetailComponent;
  let fixture: ComponentFixture<ProductEditDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductEditDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProductEditDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
