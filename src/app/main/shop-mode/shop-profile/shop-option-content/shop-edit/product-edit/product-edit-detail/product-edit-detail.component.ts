import { ProductPost } from './../../../../../../../model/interface/ProductPost';
import { Subject, takeUntil } from 'rxjs';
import { ShopModeLayoutService, shopEditLayoutState } from './../../../../../../../service/layout-service/shop-mode-layout.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-product-edit-detail',
  templateUrl: './product-edit-detail.component.html',
  styleUrls: ['./product-edit-detail.component.css']
})
export class ProductEditDetailComponent implements OnInit {
  destroy$: Subject<any>;
  post?: ProductPost;

  constructor(
    private shopModeLayoutService: ShopModeLayoutService
  ) {
    this.destroy$ = new Subject<any>();
  }

  ngOnInit(): void {
    this.shopModeLayoutService.onShopEditStateChange()
    .pipe(takeUntil(this.destroy$))
    .subscribe(res=>{
      this.post = (res as {state: shopEditLayoutState, post: ProductPost}).post;
    });
  }

  OnDestroy()
  {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
