import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopCreatePostComponent } from './shop-create-post.component';

describe('ShopCreatePostComponent', () => {
  let component: ShopCreatePostComponent;
  let fixture: ComponentFixture<ShopCreatePostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShopCreatePostComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShopCreatePostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
