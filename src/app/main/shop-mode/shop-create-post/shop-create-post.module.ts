import { ShareModule } from 'src/app/share/share.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ShopCreatePostRoutingModule } from './shop-create-post-routing.module';
import { SelectLabelsComponent } from './select-labels/select-labels.component';
import { ShopCreatePostComponent } from './shop-create-post.component';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';

@NgModule({
  declarations: [
    SelectLabelsComponent,
    ShopCreatePostComponent
  ],
  imports: [
    CommonModule,
    ShopCreatePostRoutingModule,
    ReactiveFormsModule,
    NzInputNumberModule,
    FormsModule,
    ShareModule
  ],
  exports:[
    ShopCreatePostComponent,
    SelectLabelsComponent
  ]
})
export class ShopCreatePostModule { }
