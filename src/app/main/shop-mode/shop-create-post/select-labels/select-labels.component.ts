import { ProductPost } from './../../../../model/interface/ProductPost';
import { Utility } from './../../../../utility';
import { Observable, first, switchMap, of } from 'rxjs';
import { displayLabels, ShopModeService } from './../../../../service/shop-mode-service/shop-mode.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-select-labels',
  templateUrl: './select-labels.component.html',
  styleUrls: ['./select-labels.component.css']
})
export class SelectLabelsComponent extends Utility implements OnInit {
  @Output('back') backAction: EventEmitter<any>;
  @Output('confirm') confirmAction: EventEmitter<displayLabels[]>;
  @Input('isUpdateMode') isUpdateMode?: boolean = false;

  keyword: string = "";
  searchLabelsRes$?: Observable<displayLabels[]>;
  isInit: boolean = true;

  selectedLabels: displayLabels[];
  removeDuplicateLabels = (labels: displayLabels[]) => Utility.removeDuplicateFromArray<displayLabels>(labels);

  constructor(
    private shopModeService: ShopModeService
  ) {
    super();
    this.backAction = new EventEmitter<any>();
    this.confirmAction = new EventEmitter<displayLabels[]>();
    this.selectedLabels = shopModeService.cacheSelectedLabels; //maybe empty or from update post
  }

  ngOnInit(): void {

  }

  confirm()
  {
    this.shopModeService.cacheSelectedLabels = this.selectedLabels;
    this.confirmAction.emit(this.selectedLabels);
    this.back();
  }

  clickLabel(l: displayLabels)
  {
    if(this.selectedLabels.includes(l)){
      this.selectedLabels = this.selectedLabels.filter(x=>x!=l);
    }else{
      this.selectedLabels.push(l);
    }
  }

  searchLabels(event: KeyboardEvent)
  {
    if(event.key=="Enter"){
      this.isInit = false;
      this.searchLabelsRes$ = this.shopModeService.getLabels(this.keyword)
      .pipe(
        first(),
        switchMap(res => {
          let display: displayLabels[] = [];

          res.labels.forEach(l => {
            display.push({
              type: "label",
              id: l.labelid,
              name: l.display_name
            });
          });

          res.fe_labels.forEach(l => {
            display.push({
              type: "feLabel",
              id: l.labelid,
              name: l.display_name
            });
          });

          return of(display);
        })
      );
    }
  }

  back()
  {
    this.backAction.emit(null);
  }
}
