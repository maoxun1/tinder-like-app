import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectLabelsComponent } from './select-labels.component';

describe('SelectLabelsComponent', () => {
  let component: SelectLabelsComponent;
  let fixture: ComponentFixture<SelectLabelsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectLabelsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SelectLabelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
