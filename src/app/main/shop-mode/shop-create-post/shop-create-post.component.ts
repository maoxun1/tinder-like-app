import { ModifyProductDTO } from './../../../model/DTO/ModifyProductDTO';
import { ProductPost } from 'src/app/model/interface/ProductPost';
import { ProductPicDTO } from './../../../model/DTO/ProductPicDTO';
import { Label } from './../../../model/interface/Partial';
import { first, of, switchMap, forkJoin } from 'rxjs';
import { ShopService } from './../../../service/shop-service/shop.service';
import { ProductPostDTO } from './../../../model/DTO/ProductPostDTO';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ShopModeService, displayLabels } from './../../../service/shop-mode-service/shop-mode.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-shop-create-post',
  templateUrl: './shop-create-post.component.html',
  styleUrls: ['./shop-create-post.component.css']
})
export class ShopCreatePostComponent implements OnInit {
  postForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.maxLength(60)]),
    caption: new FormControl('', [Validators.required, Validators.maxLength(300)]),
    price: new FormControl(1, [Validators.required, Validators.min(1)]),
    stock: new FormControl(0, [Validators.required, Validators.min(0)])
  });

  public get name() { return this.postForm.get('name'); }
  public get caption() { return this.postForm.get('caption'); }
  public get price() { return this.postForm.get('price'); }
  public get stock() { return this.postForm.get('stock'); }

  selectedLabels: displayLabels[] = [];
  selectedImages: File[] = [];
  firstImage?: string;

  isOpenSelectLabels: boolean = false;

  @Input('updateMode') isUpdatePostMode: boolean = false;
  @Input('update-post') updatePost?: ProductPost;

  constructor(
    private shopModeService: ShopModeService,
    private shopService: ShopService
  ) { }

  ngOnInit(): void {
    // update mode, init
    if(this.isUpdatePostMode && this.updatePost)
    {
      //form
      this.postForm.setValue({
        name: this.updatePost.name,
        caption: this.updatePost.content,
        price: this.updatePost.price,
        stock: this.updatePost.stock
      });

      //labels
      this.shopModeService.cacheSelectedLabels = this.shopModeService.parsePostLabelsToDisplayLabels(this.updatePost);
      this.selectedLabels = this.shopModeService.parsePostLabelsToDisplayLabels(this.updatePost);

      //image
      this.firstImage = this.updatePost.images[0];

    }
  }

  confirmLabel(event: displayLabels[])
  {
    this.selectedLabels = this.shopModeService.cacheSelectedLabels;
  }

  openSelectLabels()
  {
    this.isOpenSelectLabels = true;
  }
  closeSelectLabels()
  {
    this.isOpenSelectLabels = false;
  }

  setFirstImage(file: File)
  {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event) => {
      this.firstImage = (<FileReader>event.target).result?.toString();
    }
  }

  onFileSelect(event: Event)
  {
    const element = event.currentTarget as HTMLInputElement;
    let fileList: FileList|null = element.files;

    if(fileList){
      let files = Array.from(fileList);

      // guard, pics<=10
      if(fileList.length>10){
        alert("You cannot choose more than 10 pics!");
        event.preventDefault();
        return;
      }

      this.selectedImages = files;
      this.setFirstImage(this.selectedImages[this.selectedImages.length-1]);
    }
  }

  upload()
  {
    if(this.postForm.invalid)
    {
      alert("The form is invalid!");
      return;
    }

    if(!this.isUpdatePostMode && (this.selectedImages.length>10 || this.selectedImages.length<=0)){
      alert("Pics cannot be more than 10!");
      return;
    }

    var labels: Label[] = this.selectedLabels.filter(l=>l.type==="label").map(l=>{
        return {
          labelid: l.id,
          display_name: l.name
        }
    });
    var feLabels: Label[] = this.selectedLabels.filter(l=>l.type==="feLabel").map(l=>{
      return {
        labelid: l.id,
        display_name: l.name
      }
    });

    if(!this.isUpdatePostMode){
      // create post
      // upload post and pics
      this.shopService.getShop()
      .pipe(
        first(),
        switchMap(shop=>{
          var dto: ProductPostDTO = {
            shopid: shop._id,
            shopAccount: shop.shopAccount,
            name: this.name?.value,
            content: this.caption?.value,
            labels: labels,
            feLabels: feLabels,
            price: this.price?.value,
            stock: this.stock?.value
          };
          return of(dto);
        }),
        switchMap(dto=>{
          //先發文, 不用判斷是否發過, 因為沒有判斷標準
          return this.shopModeService.createProductPost(dto);
        }),
        switchMap(post=>{
          var dto: ProductPicDTO = {
            shopid: post.shopid,
            postid: post._id,
            productPics: this.selectedImages
          }
          return this.shopModeService.patchProductPostPics(dto);
        })
      ).subscribe(res=>{
        if(res){
          alert("Post successfully!");
          this.shopModeService.closerCreatePage();
        }
      });
    }else{
      //update post
      if(this.updatePost){
        var dto: ModifyProductDTO = {
          postid: this.updatePost._id,
          name: this.name?.value,
          content: this.caption?.value,
          labels: labels,
          feLabels: feLabels,
          price: this.price?.value,
          stock: this.stock?.value,
          discount: this.updatePost.discount //先放原本的
        };

        this.shopModeService.patchPost(dto).subscribe(post=>{
          if(post){
            alert("update post successfully!");
            location.reload();
          }
        });
      }
    }

  }

  back()
  {
    this.shopModeService.closerCreatePage();
  }
}
