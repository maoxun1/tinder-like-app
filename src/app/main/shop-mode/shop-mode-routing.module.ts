import { ShopModeComponent } from './shop-mode.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ShopModeComponent,
    children: [
      {
        path: '',
        redirectTo: 'shop-home',
        // no pathMatch full, coz has main prefix and so on.
      },
      {
        path: 'shop-home',
        loadChildren: () => import('./shop-home/shop-home.module').then(m => m.ShopHomeModule)
      },
      {
        path: 'shop-profile',
        loadChildren: () => import('./shop-profile/shop-profile.module').then(m => m.ShopProfileModule)
      },
      {
        path: 'shop-create-post',
        loadChildren: () => import('./shop-create-post/shop-create-post.module').then(m => m.ShopCreatePostModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopModeRoutingModule { }
